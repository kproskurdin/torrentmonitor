﻿using System;
using DataAbstraction;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using System.IO;
using Microsoft.EntityFrameworkCore.Design;

namespace Repository
{
    public interface ITorrentMonitorDataContextFactory : IDesignTimeDbContextFactory<TorrentMonitorDataContext>
    {
        IDataContext Create();
    }

    public class TorrentMonitorDataContextFactory : ITorrentMonitorDataContextFactory
    {
        public IDataContext Create()
        {
            var optionsBuilder = new DbContextOptionsBuilder<TorrentMonitorDataContext>();
            optionsBuilder.UseSqlite(DataAbstraction.CongigurationProvider.GetConfiguration().GetConnectionString("database"));

            return new TorrentMonitorDataContext(optionsBuilder.Options);
        }

        public TorrentMonitorDataContext CreateDbContext(string[] args)
        {
            var builder = new ConfigurationBuilder()
                        .SetBasePath(Path.Combine(AppContext.BaseDirectory, "../../../"))
                        .AddJsonFile("appsettings.json", optional: true);
                        //.AddJsonFile($"appsettings.{options.EnvironmentName}.json", optional: true);

            var optionsBuilder = new DbContextOptionsBuilder<TorrentMonitorDataContext>();
            optionsBuilder.UseSqlite(DataAbstraction.CongigurationProvider.GetConfiguration().GetConnectionString("database"));

            return new TorrentMonitorDataContext(optionsBuilder.Options);
        }
    }
}
