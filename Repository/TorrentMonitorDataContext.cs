﻿using DataAbstraction;
using Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;

namespace Repository
{
    public class TorrentMonitorDataContext : IdentityDbContext<ApplicationUser>, IDataContext
    {
        DbSet<Buffer> Buffers { get; set; }
        DbSet<Credential> Credentials { get; set; }
        DbSet<News> News { get; set; }
        DbSet<Notification> Notifications { get; set; }
        DbSet<Setting> Settings { get; set; }
        DbSet<Temp> Temps { get; set; }
        DbSet<Torrent> Torrents { get; set; }
        DbSet<Warning> Warnings { get; set; }
        DbSet<Watch> Watches { get; set; }
        private DbSet<StartData> StartDatas { get; set; }
        private DbSet<SearchResult> SearchResults { get; set; }
        private DbSet<SearchRequest> SearchRequests { get; set; }

        public TorrentMonitorDataContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Temp>().HasIndex(b => b.Hash).IsUnique();

            modelBuilder.Entity<Credential>().HasIndex(b => b.Tracker).IsUnique();
        }

        public List<T> GetChangedEntities<T>() where T : class
        {
            return ChangeTracker
                    .Entries<T>()
                    .Where(x => x.State == EntityState.Modified)
                    .Select(x => x.Entity)
                    .ToList();
        }

        public void EnsureSeedData(UserManager<ApplicationUser> userManager)
        {
            if (!Credentials.Any())
            {
                Credentials.Add(new Credential { Tracker = "rutracker.org"});
                Credentials.Add(new Credential { Tracker = "nnmclub.to" });
                Credentials.Add(new Credential { Tracker = "lostfilm.tv" });
                Credentials.Add(new Credential { Tracker = "novafilm.tv" });
                Credentials.Add(new Credential { Tracker = "rutor.org" });
                Credentials.Add(new Credential { Tracker = "tfile.me" });
                Credentials.Add(new Credential { Tracker = "kinozal.tv" });
                Credentials.Add(new Credential { Tracker = "anidub.com" });
                Credentials.Add(new Credential { Tracker = "baibako.tv" });
                Credentials.Add(new Credential { Tracker = "casstudio.tv" });
                Credentials.Add(new Credential { Tracker = "newstudio.tv" });
                Credentials.Add(new Credential { Tracker = "animelayer.ru" });
                Credentials.Add(new Credential { Tracker = "tracker.0day.kiev.ua" });
                Credentials.Add(new Credential { Tracker = "rustorka.com" });
                Credentials.Add(new Credential { Tracker = "pornolab.net" });
                Credentials.Add(new Credential { Tracker = "lostfilm-mirror" });
                Credentials.Add(new Credential { Tracker = "hamsterstudio.org" });
                Credentials.Add(new Credential { Tracker = "tv.mekc.info" });

                SaveChanges();
            }

            if (!Settings.Any())
            {
                Settings.Add(new Setting { Key = "send", Val = "0" });
                Settings.Add(new Setting { Key = "sendWarning", Val = "0" });
                Settings.Add(new Setting { Key = "password", Val = "1f10c9fd49952a7055531975c06c5bd8" });
                Settings.Add(new Setting { Key = "auth", Val = "1" });
                Settings.Add(new Setting { Key = "proxy", Val = "0" });
                Settings.Add(new Setting { Key = "proxyAddress", Val = "127.0.0.1:9050" });
                Settings.Add(new Setting { Key = "useTorrent", Val = "0" });
                Settings.Add(new Setting { Key = "torrentClient", Val = "" });
                Settings.Add(new Setting { Key = "torrentAddress", Val = "127.0.0.1:9091" });
                Settings.Add(new Setting { Key = "torrentLogin", Val = "" });
                Settings.Add(new Setting { Key = "torrentPassword", Val = "" });
                Settings.Add(new Setting { Key = "pathToDownload", Val = "" });
                Settings.Add(new Setting { Key = "deleteOldFiles", Val = "0" });
                Settings.Add(new Setting { Key = "serverAddress", Val = "" });
                Settings.Add(new Setting { Key = "deleteDistribution", Val = "0" });
                Settings.Add(new Setting { Key = "sendUpdate", Val = "0" });
                Settings.Add(new Setting { Key = "debug", Val = "0" });
                Settings.Add(new Setting { Key = "rss", Val = "1" });
                Settings.Add(new Setting { Key = "debugFor", Val = "" });
                Settings.Add(new Setting { Key = "httpTimeout", Val = "15" });
                Settings.Add(new Setting { Key = "sendUpdateService", Val = "" });
                Settings.Add(new Setting { Key = "sendWarningService", Val = "" });
                Settings.Add(new Setting { Key = "proxyType", Val = "15" });

                SaveChanges();
            }

            if (!Notifications.Any())
            {
                Notifications.Add(new Notification { Service = "E-mail", Type = "notification" });
                Notifications.Add(new Notification { Service = "E-mail", Type = "warning" });
                Notifications.Add(new Notification { Service = "Prowl", Type = "notification" });
                Notifications.Add(new Notification { Service = "Prowl", Type = "warning" });
                Notifications.Add(new Notification { Service = "Pushbullet", Type = "notification" });
                Notifications.Add(new Notification { Service = "Pushbullet", Type = "warning" });
                Notifications.Add(new Notification { Service = "Pushover", Type = "notification" });
                Notifications.Add(new Notification { Service = "Pushover", Type = "warning" });
                Notifications.Add(new Notification { Service = "Pushall", Type = "notification" });
                Notifications.Add(new Notification { Service = "Pushall", Type = "warning" });

                SaveChanges();
            }

            if (!Users.Any())
            {
                var user = new ApplicationUser
                {
                    UserName = "user",
                    Email = "user"
                };
                userManager.CreateAsync(user, "ChangeMe1@").Wait();
            }
        }
    }
}
