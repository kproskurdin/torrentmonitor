﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Repository.Repositories;

namespace Repository
{
    public interface IRepositoryFactory
    {
        T CreateRepository<T>() where T : IRepository;
    }

    public class RepositoryFactory : IRepositoryFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public RepositoryFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public T CreateRepository<T>() where T : IRepository
        {
            return (T)_serviceProvider.GetService(typeof(T));
        }
    }
}
