﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAbstraction;
using Domain;
using Microsoft.AspNetCore.DataProtection;

namespace Repository.Repositories
{
    public interface ICredentialsRepository : IRepository<Credential>
    {
        Credential GetById(int id);
        Credential GetByTracker(string tracker);
    }

    public class CredentialsRepository : ExtendableRepository<Credential>, ICredentialsRepository
    {
        private readonly IDataProtector _protector;

        public CredentialsRepository(ITorrentMonitorDataContextFactory dataContextFacroty, IDataProtectionProvider provider) : base(dataContextFacroty)
        {
            _protector = provider.CreateProtector("Credential_Encrypt_Decrypt");
        }

        public override IEnumerable<Credential> GetAll()
        {
            return GetQueryable().Select(Decrypt);
        }

        public Credential GetById(int id)
        {
            return GetQueryable().Where(x => x.Id == id).Select(Decrypt).FirstOrDefault();
        }

        public Credential GetByTracker(string tracker)
        {
            return GetQueryable().Where(x => x.Tracker == tracker).Select(Decrypt).FirstOrDefault();
        }

        public override void SaveChanges()
        {
            var list = DataContext.GetChangedEntities<Credential>();

            foreach (var l in list)
            {
                Encrypt(l);
            }

            base.SaveChanges();

            foreach (var l in list)
            {
                Decrypt(l);
            }
        }

        private Credential Decrypt(Credential cred)
        {
            return cred;
            /*try
            {
                cred.Log = _protector.Unprotect(cred.Log);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            try
            {
                cred.Pass = _protector.Unprotect(cred.Pass);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            try
            {
                cred.Passkey = _protector.Unprotect(cred.Passkey);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return cred;*/
        }

        private Credential Encrypt(Credential cred)
        {
           /* if (!string.IsNullOrEmpty(cred.Log))
                cred.Log = _protector.Protect(cred.Log);

            if (!string.IsNullOrEmpty(cred.Pass))
                cred.Pass = _protector.Protect(cred.Pass);

            if(!string .IsNullOrEmpty(cred.Passkey))
                cred.Passkey = _protector.Protect(cred.Passkey);
                */
            return cred;
        }
    }
}
