﻿using Domain;

namespace Repository.Repositories
{
    public interface ISearchRequestRepository : IRepository<SearchRequest>
    {
        
    }

    public class SearchRequestRepository : ExtendableRepository<SearchRequest>, ISearchRequestRepository
    {
        public SearchRequestRepository(ITorrentMonitorDataContextFactory dataContextFacroty) : base(dataContextFacroty)
        {
        }
    }
}
