﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;

namespace Repository.Repositories
{
    public interface IWarningRepository : IRepository<Warning>
    {
        IEnumerable<Warning> GetTop(int count);
    }

    public class WarningRepository : ExtendableRepository<Warning>, IWarningRepository
    {
        public WarningRepository(ITorrentMonitorDataContextFactory dataContextFacroty) : base(dataContextFacroty)
        {
        }

        public IEnumerable<Warning> GetTop(int count)
        {
            return GetQueryable().OrderByDescending(x => x.Time).Take(count);
        }
    }
}
