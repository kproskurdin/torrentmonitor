﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAbstraction;
using Domain;

namespace Repository.Repositories
{
    public interface IWatchRepository : IRepository<Watch>
    {
        bool IfExist(string tracker, string userName);
    }

    public class WatchRepository : ExtendableRepository<Watch>, IWatchRepository
    {
        public WatchRepository(ITorrentMonitorDataContextFactory dataContextFacroty) : base(dataContextFacroty)
        {
        }

        public bool IfExist(string tracker, string userName)
        {
            return GetQueryable().Any(x => x.Tracker == tracker && x.Name == userName);
        }
    }
}
