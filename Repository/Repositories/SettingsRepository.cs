﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAbstraction;
using Domain;

namespace Repository.Repositories
{
    public interface ISettingsRepository : IRepository<Setting>
    {
        Setting GetByKey(string name);
    }

    public class SettingsRepository : ExtendableRepository<Setting>, ISettingsRepository
    {
        public SettingsRepository(ITorrentMonitorDataContextFactory dataContextFacroty) : base(dataContextFacroty)
        {
        }

        public Setting GetByKey(string key)
        {
            if(string.IsNullOrEmpty(key)) return null;

            key = char.ToLower(key[0]) + key.Substring(1);

            return GetQueryable().FirstOrDefault(x => x.Key == key);
        }

    }
}
