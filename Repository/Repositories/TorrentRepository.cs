﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAbstraction;
using Domain;

namespace Repository.Repositories
{
    public interface ITorrentRepository : IRepository<Torrent>
    {
        Torrent GetById(int id);

        string[] GetAllPaths();

        bool IfThereExist(string tracker, string theme);

        bool IfSerieExist(string tracker, string name, int hd);
    }

    public class TorrentRepository : ExtendableRepository<Torrent>, ITorrentRepository
    {
        public TorrentRepository(ITorrentMonitorDataContextFactory dataContextFacroty) : base(dataContextFacroty)
        {
        }

        public Torrent GetById(int id)
        {
            return GetQueryable().FirstOrDefault(x => x.Id == id);
        }

        public string[] GetAllPaths()
        {
            return GetQueryable().Select(x => x.Path).Distinct().ToArray();
        }

        public bool IfSerieExist(string tracker, string name, int hd)
        {
            return GetQueryable().Any(x => x.Tracker == tracker && x.Name == name && x.Hd == hd);
        }

        public bool IfThereExist(string tracker, string theme)
        {
            return GetQueryable().Any(x => x.Tracker == tracker && x.Torrent_id == theme);
        }
    }
}
