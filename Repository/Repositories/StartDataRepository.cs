﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;

namespace Repository.Repositories
{
    public interface IStartDataRepository : IRepository<StartData>
    {
        StartData GetLast();
        void AddLAstRun(string log);
    }

    public class StartDataRepository : ExtendableRepository<StartData>, IStartDataRepository
    {
        public StartDataRepository(ITorrentMonitorDataContextFactory dataContextFacroty) : base(dataContextFacroty)
        {
        }

        public StartData GetLast()
        {
            return GetQueryable().OrderByDescending(x => x.StartDate).FirstOrDefault();
        }

        public void AddLAstRun(string log)
        {
            InsertAndSubmit(new StartData
            {
                ResultLog = log,
                StartDate = DateTime.Now
            });
        }
    }
}
