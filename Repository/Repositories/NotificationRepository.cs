﻿using System.Collections.Generic;
using System.Linq;
using Domain;

namespace Repository.Repositories
{
    public interface INotificationRepository : IRepository<Notification>
    {
        IEnumerable<Notification> GetByType(string t);

        Notification Get(string id, string t);
    }

    public class NotificationRepository : ExtendableRepository<Notification>, INotificationRepository
    {
        public NotificationRepository(ITorrentMonitorDataContextFactory dataContextFacroty) : base(dataContextFacroty)
        {
        }

        public IEnumerable<Notification> GetByType(string t)
        {
            return GetQueryable().Where(x => x.Type == t).AsEnumerable();
        }

       public Notification Get(string service, string t)
       {
           return GetQueryable().FirstOrDefault(x => x.Service == service && x.Type == t);
       }
    }
}
