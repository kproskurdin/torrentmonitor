﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DataAbstraction;
using Microsoft.EntityFrameworkCore;

namespace Repository.Repositories
{
    public interface IRepository
    {
        
    }

    public interface IRepository<TEntity> : IRepository where TEntity : IEntity
    {
        /*TEntity Create();*/
        
        IEnumerable<TEntity> GetAll();
        void Insert(TEntity entity);
        void InsertAndSubmit(TEntity entity);
        void Update(TEntity entity);
        void UpdateAndSubmit(TEntity entity);
        void Delete(TEntity entity);
        void DeleteAndSubmit(TEntity entity);
        void SaveChanges();
    }

    public class ExtendableRepository<TEntity> : IRepository<TEntity> where TEntity : class, IEntity
    {
        protected readonly IDataContext DataContext;

        public ExtendableRepository(ITorrentMonitorDataContextFactory dataContextFacroty)
        {
            if (dataContextFacroty == null)
            {
                throw new ArgumentNullException(nameof(dataContextFacroty));
            }

            DataContext = dataContextFacroty.Create();

            if(DataContext == null) throw new NullReferenceException($"{nameof(DataContext)} is nulls");
        }

        /*public virtual TEntity Create()
        {
            if (typeof(TEntity).IsAbstract)
            {
                throw new InvalidOperationException("The Create() method is only allowed for concrete types. For abstract types, use \"new\" to create an instance of a derived type.");
            }

            return DataContext.Set<TEntity>().;
        }*/

        public virtual IEnumerable<TEntity> GetAll()
        {
            return GetQueryable().AsEnumerable();
        }

        protected virtual IQueryable<TEntity> GetQueryable()
        {
            var set = DataContext.Set<TEntity>().AsQueryable();

            return set;
        }

        protected virtual IQueryable<TEntity> GetQueryable(params Expression<Func<TEntity, object>>[] eagerLoadProperties)
        {
            var query = GetQueryable();

            foreach (var property in eagerLoadProperties)
            {
                query = query.Include(property);
            }

            return query;
        }

        public virtual void Insert(TEntity entity)
        {
            DataContext.Set<TEntity>().Add(entity);
        }

        public virtual void InsertAndSubmit(TEntity entity)
        {
            Insert(entity);
            SaveChanges();
        }

        public virtual void Update(TEntity entity){}

        public virtual void UpdateAndSubmit(TEntity entity)
        {
            Update(entity);
            SaveChanges();
        }

        public virtual void Delete(TEntity entity)
        {
            DataContext.Set<TEntity>().Remove(entity);
        }

        public virtual void DeleteAndSubmit(TEntity entity)
        {
            Delete(entity);
            SaveChanges();
        }

        public virtual void SaveChanges()
        {
            DataContext.SaveChanges();
        }
    }
}
