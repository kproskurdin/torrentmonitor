﻿using Microsoft.Extensions.DependencyInjection;
using Repository.Repositories;

namespace Repository
{
    public static class RepositoryServices
    {
        public static void AddRepositoryServices(this IServiceCollection services)
        {
            services.AddScoped<ITorrentMonitorDataContextFactory, TorrentMonitorDataContextFactory>();
            services.AddScoped<ICredentialsRepository, CredentialsRepository>();
            services.AddScoped<ITorrentRepository, TorrentRepository>();
            services.AddScoped<IWatchRepository, WatchRepository>();
            services.AddScoped<ISettingsRepository, SettingsRepository>();
            services.AddScoped<INotificationRepository, NotificationRepository>();
            services.AddScoped<IWarningRepository, WarningRepository>();
            services.AddScoped<IStartDataRepository, StartDataRepository>();
            services.AddScoped<IRepositoryFactory, RepositoryFactory>();
            services.AddScoped<ISearchRequestRepository, SearchRequestRepository>();
        }
    }
}
