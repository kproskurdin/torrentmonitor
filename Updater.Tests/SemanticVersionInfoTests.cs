﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Xunit.Extensions;

namespace Updater.Tests
{
    public class SemanticVersionInfoTests
    {
        public static IEnumerable<object[]> ToStringReturnsCorrectStringData
        {
            get
            {
                return new[]
                {
                    new object[]
                    {
                        new SemanticVersionInfo
                        {
                            Major = 1,
                            Minor = 21,
                            Patch = 2,
                            Label = "beta4"
                        },

                        "1.21.2-beta4"
                    },

                    new object[]
                    {
                        new SemanticVersionInfo
                        {
                            Major = 1,
                            Minor = 31,
                            Patch = 2
                        },

                        "1.31.2"
                    },
                };
            }
        }

        [Theory]
        [MemberData(nameof(ToStringReturnsCorrectStringData))]
        public void ToString_returns_correct_string(SemanticVersionInfo svi, string expected)
        {
            Assert.Equal(expected, svi.ToString());
        }

        [Fact]
        public void Equals_works_fine()
        {
            var svi = new SemanticVersionInfo
            {
                Major = 1,
                Minor = 21,
                Patch = 2,
                Label = "beta4"
            };

            var svi2 = new SemanticVersionInfo
            {
                Major = 1,
                Minor = 21,
                Patch = 2,
                Label = "beta4"
            };

            Assert.True(svi == svi2);
            Assert.True(svi.Equals(svi2));
        }

        public static IEnumerable<object[]> MaxReturnsCorrectValueData
        {
            get
            {
                // Or this could read from a file. :)
                return new[]
                {
                    new object[] { new []{
                        SemanticVersionInfo.Parse("1.1.0"),
                        SemanticVersionInfo.Parse("1.1.1"),
                        SemanticVersionInfo.Parse("1.0.10")},

                        SemanticVersionInfo.Parse("1.1.1")},

                     new object[] { new []{
                        SemanticVersionInfo.Parse("1.0.0"),
                        SemanticVersionInfo.Parse("1.0.7"),
                        SemanticVersionInfo.Parse("1.0.10")},

                        SemanticVersionInfo.Parse("1.0.10")},

                    new object[] { new []{
                        SemanticVersionInfo.Parse("0.0.0-alpha1"),
                        SemanticVersionInfo.Parse("0.0.0-alpha10")},

                        SemanticVersionInfo.Parse("0.0.0-alpha10")},


                    new object[] { new []{
                        SemanticVersionInfo.Parse("0.0.0-alpha5"),
                        SemanticVersionInfo.Parse("0.0.0-alpha7"),
                        SemanticVersionInfo.Parse("0.0.0-alpha10")},

                        SemanticVersionInfo.Parse("0.0.0-alpha10")},

                    new object[] { new []{
                        SemanticVersionInfo.Parse("0.0.0-alpha35"),
                        SemanticVersionInfo.Parse("0.0.0-alpha27"),
                        SemanticVersionInfo.Parse("0.0.0-alpha190")},

                        SemanticVersionInfo.Parse("0.0.0-alpha190")},

                    new object[] { new []{
                        SemanticVersionInfo.Parse("0.0.0-beta2"),
                        SemanticVersionInfo.Parse("0.0.0-alpha7"),
                        SemanticVersionInfo.Parse("0.0.0-alpha10")},

                        SemanticVersionInfo.Parse("0.0.0-beta2")}
                };
            }
        }

        [Theory]
        [MemberData(nameof(MaxReturnsCorrectValueData))]
        public void Max_returns_correct_value(SemanticVersionInfo[] list, SemanticVersionInfo expected )
        {
            Assert.Equal(expected, list.Max());
        }
    }
}
