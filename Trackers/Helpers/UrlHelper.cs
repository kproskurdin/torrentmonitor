﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Trackers.Helpers
{
    public interface IUrlHelper
    {
        Task<HtmlResult> Get(string url, Dictionary<string, string> headers = null, string cookie = null);
        Task<HtmlResult> Get(string url, Dictionary<string, string> headers = null, Dictionary<string, string> cookies = null);

        Task<HtmlResult> Post(string url, HttpContent content, Dictionary<string, string> headers = null, string cookie = null);
        Task<HtmlResult> Post(string url, HttpContent content, Dictionary<string, string> headers = null, Dictionary<string, string> cookies = null);

        Task<HtmlResult> GetContent(string url, HttpMethod method, HttpContent content, Dictionary<string, string> headers, string cookie);
        Task<HtmlResult> GetContent(string url, HttpMethod method, HttpContent content, Dictionary<string, string> headers, Dictionary<string, string> cookies);

        Dictionary<string, string> GonvertStringCookiesToDictionary(string cookie);
        string GonvertDictionaryCookiesToString(Dictionary<string, string> cookie);
    }

    public class UrlHelper : IUrlHelper
    {
        public async Task<HtmlResult> Get(string url, Dictionary<string, string> headers = null, string cookie = null)
        {
            return await Get(url, headers, GonvertStringCookiesToDictionary(cookie));
        }

        public async Task<HtmlResult> Get(string url, Dictionary<string, string> headers = null, Dictionary<string, string> cookies = null)
        {
            return await GetContent(url, HttpMethod.Get, null, headers, cookies);
        }

        public async Task<HtmlResult> Post(string url, HttpContent content, Dictionary<string, string> headers = null, string cookie = null)
        {
            return await Post(url, content, headers, GonvertStringCookiesToDictionary(cookie));
        }

        public async Task<HtmlResult> Post(string url, HttpContent content, Dictionary<string, string> headers = null, Dictionary<string, string> cookies = null)
        {
            return await GetContent(url, HttpMethod.Post, content, headers, cookies);
        }

        public Dictionary<string, string> GonvertStringCookiesToDictionary(string cookie)
        {
            if (!string.IsNullOrEmpty(cookie))
            {
                return cookie.Split(';').ToDictionary(x => x.Split('=')[0], x => x.Split('=')[1]);
            }
            return new Dictionary<string, string>();
        }
        public string GonvertDictionaryCookiesToString(Dictionary<string, string> cookie)
        {
            return string.Join(";", cookie.Select(x => $"{x.Key}={x.Value}"));
        }

        public async Task<HtmlResult> GetContent(string url, HttpMethod method, HttpContent content, Dictionary<string, string> headers, string cookie)
        {
            return await GetContent(url, method, content, headers, GonvertStringCookiesToDictionary(cookie));
        }

        public async Task<HtmlResult> GetContent(string url, HttpMethod method, HttpContent content, Dictionary<string, string> headers, Dictionary<string, string> cookies)
        {
            HtmlResult ret = new HtmlResult();

            var address = new Uri(url);
            var baseAddress = new Uri(address.Scheme + "://" + address.Host);
            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
                {
                    if (cookies != null)
                    {
                        foreach (var c in cookies)
                        {
                            cookieContainer.Add(baseAddress, new Cookie(c.Key, c.Value));
                        }
                    }

                    if (headers != null)
                    {
                        foreach (var header in headers)
                        {
                            client.DefaultRequestHeaders.Add(header.Key, header.Value);
                        }
                    }

                    client.DefaultRequestHeaders.ConnectionClose = true;

                    client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "text/html,application/xhtml+xml,application/xml");
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Encoding", "gzip, deflate");
                    client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0");
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Charset", "ISO-8859-1");

                    HttpResponseMessage result;
                    if (method == HttpMethod.Post)
                    {
                        result = await client.PostAsync(address, content);
                    }
                    else
                    {
                        result = await client.GetAsync(address);
                    }

                    result.EnsureSuccessStatusCode();

                    ret.Headers = result.Headers.ToDictionary(x => x.Key, x => string.Join(";", x.Value));

                    ret.Content = Read(result);

                    foreach (Cookie c in handler.CookieContainer.GetCookies(address))
                    {
                        if (!ret.Cookies.ContainsKey(c.Name))
                        {
                            ret.Cookies.Add(c.Name, c.Value);
                        }
                        else
                        {
                            ret.Cookies[c.Name] = c.Value;
                        }
                    }
                    
                }
            }

            return ret;
        }

        private string Read(HttpResponseMessage result)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            //try to detect encoding for html
            Encoding encoding = !string.IsNullOrEmpty(result.Content.Headers.ContentType?.CharSet) 
                                ? Encoding.GetEncoding(result.Content.Headers.ContentType.CharSet.Replace("cp", "windows-"))
                                : null;

            if(result.Content.Headers.ContentType != null && result.Content.Headers.ContentType.MediaType.ToLower().Contains("xml"))
            {
                encoding = Encoding.UTF8;
            }

            var task = result.Content.ReadAsStreamAsync();
            task.Wait();

            using (var responseStream = task.Result)
            {
                if (result.Content.Headers.ContentEncoding.FirstOrDefault() == "gzip")
                {
                    using (var decompressedStream = new GZipStream(responseStream, CompressionMode.Decompress))
                    {
                        if (encoding != null)
                        {
                            using (var streamReader = new StreamReader(decompressedStream, encoding))
                            {
                                return streamReader.ReadToEnd();
                            }
                        }
                        else
                        {
                            using (var streamReader = new StreamReader(decompressedStream))
                            {
                                return streamReader.ReadToEnd();
                            }
                        }
                    }
                }
                else if(encoding != null)
                {
                    using (var streamReader = new StreamReader(responseStream, encoding))
                    {
                        return streamReader.ReadToEnd();
                    }
                }
                else
                {
                    MemoryStream ms = new MemoryStream();
                    responseStream.CopyTo(ms);
                    var data = ms.ToArray();

                    return Convert.ToBase64String(data);
                }
            }
        }
    }

    public class HtmlResult
    {
        /// <summary>
        /// HTML or Base64 Encoded file, depends on server response
        /// </summary>
        public string Content { get; set; }
        public Dictionary<string, string> Headers { get; set; } = new Dictionary<string, string>();
        public Dictionary<string, string> Cookies { get; set; } = new Dictionary<string, string>();
    }
}
