﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Trackers.Helpers;

namespace Trackers
{
    public static class TrackersServices
    {
        public static void AddTrackersServices(this IServiceCollection services)
        {
            services.AddTransient<ITorrentEngineActivator, TorrentEngineActivator>();
            services.AddTransient<IUrlHelper, UrlHelper>();
        }
    }
}
