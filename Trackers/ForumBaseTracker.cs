﻿using System;
using System.Collections.Generic;
using Domain;
using Repository.Repositories;
using TorrentClients;
using Trackers.Helpers;
using System.Threading.Tasks;

namespace Trackers
{
    public abstract class ForumBaseTracker : BaseTracker
    {
        public ForumBaseTracker(IUrlHelper urlHelper, ICredentialsRepository credentialsRepository,
            ISettingsRepository settingsRepository, ITorrentClientFactory torrentClientFactory) 
            : base(urlHelper, credentialsRepository, settingsRepository, torrentClientFactory)
        {
        }

        protected abstract Task<DateTime> GetLastUpdateDate(Torrent torrent);

        protected abstract Task<string> GetTorrentTitle(Torrent torrent);

        public virtual List<SearchResult> SearchByThemeName()
        {
            return new List<SearchResult>(); 
        }

        public override async Task ProcessedTorrent(Torrent torrent)
        {
            Cookies = UrlHelper.GonvertStringCookiesToDictionary(Credentials.Cookie);

            if (!(await CheckCookies()))
            {
                Cookies = await GetCookies();
                Credentials.Cookie = UrlHelper.GonvertDictionaryCookiesToString(Cookies);
            }

            var dt = await GetLastUpdateDate(torrent);

            if (dt == torrent.Timestamp) return;

            var torrentFile = await GetTorrentFile(torrent);

            if (torrent.Auto_update)
            {
                torrent.Name = await GetTorrentTitle(torrent);
            }

            torrent.Timestamp = dt;

            SaveTorrent(torrent, torrentFile);
        }

        public class SearchResult
        {
            public string Name { get; set; }

            public string Url { get; set; }

            public DateTime AddedDate { get; set; }
        }
    }
}
