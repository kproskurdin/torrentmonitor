﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Domain;
using Repository.Repositories;
using TorrentClients;
using Trackers.Helpers;
using Transmission.API.RPC;
using Transmission.API.RPC.Entity;

namespace Trackers
{
    public abstract class BaseTracker
    {
        protected readonly IUrlHelper UrlHelper;

        protected Dictionary<string, string> Cookies { get; set; } = new Dictionary<string, string>();

        private Credential _credentials = null;
        protected Credential Credentials => _credentials ?? (_credentials = CredentialsRepository.GetByTracker(Tracker));

        private readonly ITorrentClientFactory _torrentClientFactory;

        public abstract string Tracker { get; }

        protected readonly ICredentialsRepository CredentialsRepository;
        protected readonly ISettingsRepository SettingsRepository;

        protected BaseTracker(IUrlHelper urlHelper, ICredentialsRepository credentialsRepository,
            ISettingsRepository settingsRepository, ITorrentClientFactory torrentClientFactory)
        {
            UrlHelper = urlHelper;
            CredentialsRepository = credentialsRepository;
            SettingsRepository = settingsRepository;
            _torrentClientFactory = torrentClientFactory;
        }

        public abstract Task ProcessedTorrent(Torrent torren);

        protected abstract Task<bool> CheckCookies();

        protected abstract Task<Dictionary<string, string>> GetCookies();

        protected abstract Task<string> GetTorrentFile(Torrent torrent);

        public virtual bool CheckRule(string data)
        {
            return true;
        }

        protected virtual void SaveTorrent(Torrent torren, string torrentFileBase64)
        {
            if (string.IsNullOrEmpty(torren.Path))
            {
                throw new Exception($"Torrent path cannton be empty, torrent id = {torren.Id}");
            }

            var torrenFileDir = $"{torren.Path.Trim('\\')}";

            if (!Directory.Exists(torrenFileDir))
            {
                Directory.CreateDirectory(torrenFileDir);
            }

            if (SettingsRepository.GetByKey("useTorrent").Val == "1")
            {
                var client = _torrentClientFactory.Get(SettingsRepository.GetByKey("torrentClient").Val);

                client.AddTorrent(torrentFileBase64, torrenFileDir);
            }
            else
            {
                var torrenFileName = $"{torrenFileDir}\\{torren.Tracker}_{torren.Torrent_id}.torrent";
                File.WriteAllBytes(torrenFileName, Convert.FromBase64String(torrentFileBase64));
            }
        }
    }
}
