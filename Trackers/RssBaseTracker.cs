﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Domain;
using Repository.Repositories;
using TorrentClients;
using Trackers.Helpers;

namespace Trackers
{
    public abstract class RssBaseTracker : BaseTracker
    {
        public RssBaseTracker(IUrlHelper urlHelper, ICredentialsRepository credentialsRepository,
            ISettingsRepository settingsRepository, ITorrentClientFactory torrentClientFactory)
            : base(urlHelper, credentialsRepository, settingsRepository, torrentClientFactory)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>episode nuber in format S00E00</returns>
        protected abstract Task<string> GetLatestEpisodeNumber(Torrent torrent);

        public override async Task ProcessedTorrent(Torrent torrent)
        {
            Cookies = UrlHelper.GonvertStringCookiesToDictionary(Credentials.Cookie);

            if (! (await CheckCookies()))
            {
                Cookies = await GetCookies();
                Credentials.Cookie = UrlHelper.GonvertDictionaryCookiesToString(Cookies);
            }

            var newEpisodeInfoStr = await GetLatestEpisodeNumber(torrent);

            if (string.IsNullOrEmpty(newEpisodeInfoStr)) return;

            var newEpisodeInfo = Regex.Match(newEpisodeInfoStr, @"S(\d{2})E(\d{2})");

            if (!newEpisodeInfo.Success)
            {
                return;
            }

            int newSeason = int.Parse(newEpisodeInfo.Groups[1].Value);
            int newEpisode = int.Parse(newEpisodeInfo.Groups[2].Value);

            int currentEpisode = 0;
            int currentSeason = 0;

            if (!string.IsNullOrEmpty(torrent.Ep))
            {
                var cep = Regex.Match(torrent.Ep, @"S(\d{2})E(\d{2})");
                if (cep.Success)
                {
                    currentSeason = int.Parse(cep.Groups[1].Value);
                    currentEpisode = int.Parse(cep.Groups[2].Value);
                }
            }

            if (newSeason > currentSeason || (newSeason == currentSeason && newEpisode > currentEpisode))
            {
                var torrentBase64 = await GetTorrentFile(torrent);

                torrent.Ep = $"S{newSeason.ToString("0#")}E{newEpisode.ToString("0#")}";
                torrent.Timestamp = DateTime.Now;

                SaveTorrent(torrent, torrentBase64);
            }
        }
    }
}
