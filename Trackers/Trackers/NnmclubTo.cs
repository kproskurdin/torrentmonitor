using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http;
using System.Text.RegularExpressions;
using Domain;
using Repository.Repositories;
using TorrentClients;
using Trackers.Helpers;
using System.Threading.Tasks;

namespace Trackers.Trackers
{
    [Tracker(TrackerName)]
    public class NnmclubTo : ForumBaseTracker
    {
        private const string TrackerName = "nnmclub.to";
        public override string Tracker => TrackerName;

        public NnmclubTo(IUrlHelper urlHelper, ICredentialsRepository credentialsRepository, 
            ISettingsRepository settingsRepository, ITorrentClientFactory torrentClientFactory) 
            : base(urlHelper, credentialsRepository, settingsRepository, torrentClientFactory)
        {
        }

        public override bool CheckRule(string data)
        {
            return Regex.Match(data, @"\d+").Success;
        }

        protected override async Task<bool> CheckCookies()
        {
            var res = await UrlHelper.GetContent("http://nnm-club.me/forum/index.php", HttpMethod.Get, null, null, Cookies);

            return Regex.Match(res.Content, @"login\.php\?logout=true").Success;
        }

        protected override async Task<Dictionary<string, string>> GetCookies()
        {
            if (Credentials == null) throw new Exception("credential_miss");

            var content = new FormUrlEncodedContent(new[]
                        {
                            new KeyValuePair<string, string>("username", Credentials.Log),
                            new KeyValuePair<string, string>("password", Credentials.Pass),
                            new KeyValuePair<string, string>("login", "%C2%F5%EE%E4")
                        });

            var res = await UrlHelper.Post("http://nnm-club.me/forum/login.php", content, null, cookie: null);

            if (string.IsNullOrEmpty(res.Content)) throw new Exception("not_available");

            if (Regex.Match(res.Content, @"login\.php\?redirect=").Success)
            {
                throw new Exception("credential_wrong");
            }

            return res.Cookies;
        }

        protected override async Task<DateTime> GetLastUpdateDate(Torrent torrent)
        {
            var page = await UrlHelper.Get($"http://nnm-club.me/forum/viewtopic.php?t={torrent.Torrent_id}", null, Cookies);

            if (string.IsNullOrEmpty(page.Content)) throw new Exception("not_available");

            //var datePattern = "<td class=\"genmed\">&nbsp;???????????????:&nbsp;</td>\n.*<td class=\"genmed\">&nbsp;(\\d{2}.*\\d{4}.*\\d{2}:\\d{2}:\\d{2})</td>";
            var datePattern = "<td class=\"genmed\">&nbsp;\\S{15}:&nbsp;</td>\n.*<td class=\"genmed\">&nbsp;(\\d{2}.*\\d{4}.*\\d{2}:\\d{2}:\\d{2})</td>";
            var res = Regex.Match(page.Content, datePattern, RegexOptions.Multiline);

            if (!res.Success) throw new Exception("not_available");

            //<td class="genmed">&nbsp;???????????????:&nbsp;</td>
            //<td class="genmed">&nbsp;12 ??? 2016 11:29:14</td>
            var dt = DateTime.ParseExact(res.Groups[1].Value, "dd MMM yyyy HH:mm:ss", new CultureInfo("ru-RU"));

            return dt;
        }

        protected override async Task<string> GetTorrentFile(Torrent torrent)
        {
            var page = await UrlHelper.Get($"http://nnm-club.me/forum/viewtopic.php?t={torrent.Torrent_id}", null, Cookies);

            var downloadId = Regex.Match(page.Content, @"download\.php\?id=(\d{6,8})").Groups[1].Value;
            /*
            var sid = Cookies["phpbb2mysql_4_sid"];
            var uid = Cookies["_ym_uid"];

            return UrlHelper.Get($"https://nnm-club.ws/download.php?csid={sid}&uid={uid}&id={downloadId}", null, Cookies).Content;*/

            return (await UrlHelper.Get($"http://nnmclub.to/forum/download.php?id={downloadId}", null, Cookies)).Content;
        }

        protected override async Task<string> GetTorrentTitle(Torrent torrent)
        {
            var page = await UrlHelper.Get($"http://nnm-club.me/forum/viewtopic.php?t={torrent.Torrent_id}", null, Cookies);
            return Regex.Match(page.Content, "<title>(.*)</title>").Groups[1].Value;
        }
    }
}