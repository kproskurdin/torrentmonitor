﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http;
using System.Text.RegularExpressions;
using Domain;
using Repository.Repositories;
using TorrentClients;
using Trackers.Helpers;
using System.Threading.Tasks;

namespace Trackers.Trackers
{
    [Tracker(TrackerName)]
    public class RutrackerOrg : ForumBaseTracker
    {
        private const string TrackerName = "rutracker.org";

        public override string Tracker => TrackerName;

        public RutrackerOrg(IUrlHelper urlHelper, ICredentialsRepository credentialRepository,
            ISettingsRepository settingsRepository, ITorrentClientFactory torrentClientFactory) 
            : base(urlHelper, credentialRepository, settingsRepository, torrentClientFactory)
        {
        }

        protected override async Task<string> GetTorrentTitle(Torrent torrent)
        {
            var page = await UrlHelper.Get($"https://rutracker.org/forum/viewtopic.php?t={torrent.Torrent_id}", null, Cookies);
            return Regex.Match(page.Content, "<title>(.*)</title>").Groups[1].Value;
        }

        protected override async Task<bool> CheckCookies()
        {
            var res= await UrlHelper.GetContent("https://rutracker.org/forum/index.php", HttpMethod.Get, null, null, Cookies);

            return Regex.Match(res.Content, "Вы зашли как:").Success;
        }

        public override bool CheckRule(string data)
        {
            return Regex.Match(data, @"\d+").Success;
        }

        protected override async Task<Dictionary<string, string>> GetCookies()
        {
            if(Credentials == null) throw new Exception("credential_miss");

            var content = new FormUrlEncodedContent(new[]
                        {
                            new KeyValuePair<string, string>("login_username", Credentials.Log),
                            new KeyValuePair<string, string>("login_password", Credentials.Pass),
                            new KeyValuePair<string, string>("login", "%C2%F5%EE%E4")
                        });

            var res = await UrlHelper.Post("https://rutracker.org/forum/login.php", content, null, cookie:null);

            if(string.IsNullOrEmpty(res.Content)) throw new Exception("not_available");

            if (Regex.Match(res.Content, @"profile\.php\?mode=register").Success)
            {
                throw new Exception("credential_wrong");
            }

            if (res.Cookies.ContainsKey("bb_session"))
            {
                return res.Cookies;
            }

            throw new Exception("not_available");
        }

        protected override async Task<DateTime> GetLastUpdateDate(Torrent torrent)
        {
            var page = await UrlHelper.Get($"https://rutracker.org/forum/viewtopic.php?t={torrent.Torrent_id}", null, Cookies);

            if (string.IsNullOrEmpty(page.Content)) throw new Exception("not_available");

            var res = Regex.Match(page.Content, @"<td style=""width: 15%;"">Зарегистрирован:</td>\s*<td style=""width: 70%; padding: 5px;"">\s*<ul class=""inlined middot-separated"">\s*<li>\s*(.+)\s*</li>");

            if (!res.Success) throw new Exception("not_available");

            //Database::clearWarnings($tracker);
            //format sample "03-Мар-16 20:41"
            var dt = DateTime.ParseExact(res.Groups[1].Value.Trim(), "dd-MMM-yy HH:mm", new CultureInfo("ru-RU"));

            return dt;
        }

        protected override async Task<string> GetTorrentFile(Torrent torrent)
        {
            return (await UrlHelper.Get($"https://rutracker.org/forum/dl.php?t={torrent.Torrent_id}", null, Cookies)).Content;
        }
    }
}
