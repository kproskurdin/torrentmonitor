using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using Domain;
using Repository.Repositories;
using TorrentClients;
using Trackers.Helpers;
using System.Threading.Tasks;

namespace Trackers.Trackers
{
    [Tracker(TrackerName)]
    public class LostfilmTv : RssBaseTracker
    {
        public LostfilmTv(IUrlHelper urlHelper, ICredentialsRepository credentialsRepository, 
            ISettingsRepository settingsRepository, ITorrentClientFactory torrentClientFactory) 
            : base(urlHelper, credentialsRepository, settingsRepository, torrentClientFactory)
        {
        }

        private const string TrackerName = "lostfilm.tv";

        public override string Tracker => TrackerName;

        private async Task<string> GetTorrentUrl(Torrent torrent)
        {
            var xml = await GetXml();

            var xmlDocumet = XElement.Load(new StringReader(xml));

            string quality;
            switch (torrent.Hd)
            {
                case 1:
                    quality = ".mkv";
                    break;
                case 2:
                    quality = ".mp4";
                    break;
                case 0:
                default:
                    quality = ".avi";
                    break;
            }

            var xmlName = torrent.Name.Replace(" ", ".").Replace("'", string.Empty);

            var url = (from el in xmlDocumet.Element("channel").Elements("item")
                       where el.Element("link") != null && el.Element("link").Value.Contains(xmlName) && el.Element("link").Value.Contains(quality)
                       select el.Element("link").Value).FirstOrDefault();

            return url != null ? url.Replace("www.", "old.") : null;
        }

        protected override async Task<string> GetLatestEpisodeNumber(Torrent torrent)
        {
            var url = await GetTorrentUrl(torrent);

            if (string.IsNullOrEmpty(url)) return null;

            var newEpisodeInfo = Regex.Match(url, @"(S\d{2}E\d{2})");

            if (!newEpisodeInfo.Success)
            {
                return null;
            }

            return newEpisodeInfo.Groups[1].Value;
        }

        protected async Task<string> GetXml()
        {
            return (await UrlHelper.GetContent("http://old.lostfilm.tv/rssdd.xml", HttpMethod.Get, null, null, Cookies)).Content;
        }

        protected override async Task<bool> CheckCookies()
        {
            var res = await UrlHelper.GetContent("http://old.lostfilm.tv/", HttpMethod.Get, null, null, Cookies);

            return Regex.Match(res.Content, @"\S{6}, <span class=\""wh\"">.*\s<!-- \(ID: .*\) --><\/span><br \/>").Success;
        }

        protected override async Task<Dictionary<string, string>> GetCookies()
        {
            var content = new FormUrlEncodedContent(new[]
                        {
                            new KeyValuePair<string, string>("login", Credentials.Log),
                            new KeyValuePair<string, string>("password", Credentials.Pass),
                            new KeyValuePair<string, string>("module", "1"),
                            new KeyValuePair<string, string>("target", "https%3A%2F%2Flostfilm.tv%2F"),
                            new KeyValuePair<string, string>("repage", "user"),
                            new KeyValuePair<string, string>("act", "login")
                        });

            var loginRes = await UrlHelper.Post("http://login1.bogi.ru/login.php?referer=https%3A%2F%2Fold.lostfilm.tv%2F", content, null, cookie: null);

            var url = Regex.Match(loginRes.Content, @"action=\""(.*?)\""").Groups[1].Value;
            var names = Regex.Matches(loginRes.Content, @"name=""(.*?)""");
            var values = Regex.Matches(loginRes.Content, @"value=""(.*?)""");

            var data = new List<KeyValuePair<string, string>>();

            for(var i = 1 ; i < names.Count - 1; i++)
            {
                data.Add(new KeyValuePair<string, string>(names[i].Groups[1].Value, values[i - 1].Groups[1].Value));
            }

            content = new FormUrlEncodedContent(data.ToArray());

            loginRes = await UrlHelper.Post(url, content, null, cookie: null);

            var cookies = loginRes.Cookies;

            var myPageHtml = (await UrlHelper.GetContent("https://old.lostfilm.tv/my.php", HttpMethod.Get, null, null, cookies)).Content;
            var usess = Regex.Match(myPageHtml, @"<td align=""left"">(.*)<br >").Groups[1].Value;
            cookies.Add("usess", usess);

            return cookies;
        }

        protected override async Task<string> GetTorrentFile(Torrent torrent)
        {
            var url = await GetTorrentUrl(torrent);
            var ret = await UrlHelper.Post(url, null, null, Cookies);
            return ret.Content;
        }
    }
}