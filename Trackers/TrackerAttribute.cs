﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Trackers
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
    public class TrackerAttribute :Attribute
    {
        public string Name { get; set; }

        public TrackerAttribute(string name)
        {
            Name = name;
        }
    }
}
