﻿using System;
using System.Linq;
using System.Reflection;
using Repository.Repositories;
using Trackers.Helpers;

namespace Trackers
{
    public interface ITorrentEngineActivator
    {
        bool Exist(string name);
        Type GetType(string name);
        BaseTracker GeTorrentEngine(string name);
    }

    public class TorrentEngineActivator : ITorrentEngineActivator
    {
        private readonly IServiceProvider _serviceProvider;

        public TorrentEngineActivator(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public bool Exist(string name)
        {
            return GetType(name) != null;
        }

        public Type GetType(string name)
        {
            var hts = typeof(TorrentEngineActivator).GetTypeInfo().Assembly
                    .GetTypes()
                    .Where(t => t != null && t.GetTypeInfo().BaseType != null && 
                    (t.GetTypeInfo().BaseType == typeof(ForumBaseTracker)
                    || t.GetTypeInfo().BaseType == typeof(RssBaseTracker)
                    ));

            return (from ht in hts
                                let ca = ht.GetTypeInfo().CustomAttributes
                                from ana in ca
                                where ana != null && ana.AttributeType == typeof(TrackerAttribute) && ana.ConstructorArguments.Any(x => x.Value.ToString() == name)
                                select ht).FirstOrDefault();
        }

        public BaseTracker GeTorrentEngine(string name)
        {

            var handlerType = GetType(name);

            if (handlerType == null) return null;



            var ctors = handlerType.GetConstructors();

            if(ctors == null) throw new Exception($"No public constructors for type {handlerType.FullName}");


            foreach (var ctor in ctors)
            {
                var ctorParams = ctor.GetParameters().ToList();

                if (ctorParams.Count == 0)
                {
                    return (BaseTracker)Activator.CreateInstance(handlerType);
                }

                return (BaseTracker)Activator.CreateInstance(handlerType, ctorParams.Select(x => _serviceProvider.GetService(x.ParameterType)).ToArray());
            }

            return null;
            //return (BaseTracker)Activator.CreateInstance(handlerType, _serviceProvider.GetService(typeof(IUrlHelper)), _serviceProvider.GetService(typeof(ICredentialsRepository)));

        }
    }
}