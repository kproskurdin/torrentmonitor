﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain;
using Repository;
using Repository.Repositories;
using Trackers;
using System.Threading.Tasks;

namespace Scheduler
{
    public interface ISchedulerEngine
    {
        Task<List<string>> Run();
    }

    public class SchedulerEngine : ISchedulerEngine
    {
        private readonly ITorrentRepository _torrentRepository;
        private readonly ITorrentEngineActivator _engineActivator;
        private readonly IWarningRepository _warningRepository;
        private readonly IStartDataRepository _startDataRepository;
        private readonly IRepositoryFactory _repositoryFactory;

        public SchedulerEngine(ITorrentRepository torrentRepository, ITorrentEngineActivator engineActivator, 
            IWarningRepository warningRepository, IStartDataRepository startDataRepository, IRepositoryFactory repositoryFactory)
        {
            _torrentRepository = torrentRepository;
            _engineActivator = engineActivator;
            _warningRepository = warningRepository;
            _startDataRepository = startDataRepository;
            _repositoryFactory = repositoryFactory;
        }

        private List<string> Messages = new List<string>();

        public async Task<List<string>> Run()
        {
            Messages.Clear();

            AddMessage("Execution Starrted");

            var torrentIds = _torrentRepository.GetAll().Select(x => x.Id).ToList();

            foreach (var torrentId in torrentIds)
            {
                //To avoid saved data on exception get it from different data context
                var rep = _repositoryFactory.CreateRepository<ITorrentRepository>();

                var torrent = rep.GetById(torrentId);
                AddMessage($"Starting torrent {torrent.Name}");
                try
                {
                    var torrentEngine = _engineActivator.GeTorrentEngine(torrent.Tracker);

                    await torrentEngine.ProcessedTorrent(torrent);

                    rep.SaveChanges();
                }
                catch (Exception ex)
                {
                    var reason = ExceptionFullReason(ex);

                    _warningRepository.InsertAndSubmit(new Warning()
                    {
                        Where = torrent.Tracker,
                        Time = DateTime.Now,
                        Reason = reason
                    });
                    AddMessage($"Error while processing torrent {torrent.Name}, error: {ex.Message}");
                }
                AddMessage($"Torrent finished {torrent.Name}");
            }
            AddMessage("Execution Ended");

            _startDataRepository.AddLAstRun(string.Join("\n", Messages));

            return Messages;
        }

        private string ExceptionFullReason(Exception ex)
        {
            var reason = $"Message: {ex.Message}\nStackTrace: {ex.StackTrace}";

            if (ex.InnerException != null)
            {
                reason += "\nInner Exception:\n";
                reason += ExceptionFullReason(ex.InnerException);
            }

            return reason;
        }

        private void AddMessage(string message)
        {
            Messages.Add($"{DateTime.Now.ToString("yyyy-MM-d hh:mm:ss")} - {message}");
        }
    }
}
