﻿using System;
using DataAbstraction;
using Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Repository;
using Trackers;
using Microsoft.EntityFrameworkCore;
using TorrentClients;
using Microsoft.Extensions.Configuration.UserSecrets;

[assembly: UserSecretsId("aspnet-Site-9780e59f-cdbf-4739-bb88-8770e524e19c")]

namespace Scheduler
{
    public class Program
    {
        public static IServiceProvider ServiceProvider;
        public static IConfigurationRoot Configuration;

        private static void InitServiceProvider()
        {
            var services = new ServiceCollection();

            var builder = new ConfigurationBuilder()
               .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
            CongigurationProvider.SetConfiguration(Configuration);

            services.AddDbContext<TorrentMonitorDataContext>(options =>
               options.UseSqlite(Configuration.GetConnectionString("database")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<TorrentMonitorDataContext>()
                .AddDefaultTokenProviders();

            services.AddDataProtection();

            services.AddRepositoryServices();
            services.AddTrackersServices();
            services.AddSchedulerServices();
            services.AddTorrentClientServices();

            ServiceProvider = services.BuildServiceProvider();
        }

        public static void Main(string[] args)
        {
            try
            {
                InitServiceProvider();

                var ctx = ServiceProvider.GetService<TorrentMonitorDataContext>();
                ctx.Database.Migrate();
                ctx.EnsureSeedData(ServiceProvider.GetService<UserManager<ApplicationUser>>());

                var logs = ServiceProvider.GetService<ISchedulerEngine>().Run().Result;

                logs.ForEach(x => Console.WriteLine(x));
            }
            catch (Exception ex)
            {
                PrintError(ex);
            }
        }

        private static void PrintError(Exception ex)
        {
            if (ex == null) return;

            Console.WriteLine(ex.Message);
            Console.WriteLine(ex.StackTrace);

            PrintError(ex.InnerException);
        }
    }
}
