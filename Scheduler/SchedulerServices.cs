﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace Scheduler
{
    public static class SchedulerServices
    {
        public static void AddSchedulerServices(this IServiceCollection services)
        {
            services.AddScoped<ISchedulerEngine, SchedulerEngine>();
        }
    }
}
