﻿using System;
using Repository.Repositories;

namespace TorrentClients
{
    public interface ITorrentClientFactory
    {
        ITorrentClient Get(string name);
    }

    public class TorrentClientFactory : ITorrentClientFactory
    {
        private readonly ISettingsRepository _settingsRepository;

        public TorrentClientFactory(ISettingsRepository settingsRepository)
        {
            _settingsRepository = settingsRepository;
        }

        public ITorrentClient Get(string name)
        {
            switch (name.ToLower())
            {
                case "transmission":
                    return new Clients.Transmission(_settingsRepository);
                default:
                    throw new NotImplementedException($"No torrent client for name '{name}'");
            }
        }
    }
}
