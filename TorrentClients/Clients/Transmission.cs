﻿using Repository.Repositories;
using Transmission.API.RPC;
using Transmission.API.RPC.Entity;

namespace TorrentClients.Clients
{
    public class Transmission : ITorrentClient
    {
        private readonly ISettingsRepository _settingsRepository;

        public Transmission(ISettingsRepository settingsRepository)
        {
            _settingsRepository = settingsRepository;
        }

        public void AddTorrent(string torrentFileBase64, string downloadFolder)
        {
            var trClient = new Client(_settingsRepository.GetByKey("torrentAddress").Val);

            trClient.TorrentAdd(new NewTorrent()
            {
                DownloadDirectory = downloadFolder,
                Metainfo = torrentFileBase64
            });
        }
    }
}
