﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TorrentClients
{
    public interface ITorrentClient
    {
        void AddTorrent(string torrentFileBase64, string downloadFolder);
    }
}
