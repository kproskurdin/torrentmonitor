﻿using Microsoft.Extensions.DependencyInjection;

namespace TorrentClients
{
    public static class TorrentClientServices
    {
        public static void AddTorrentClientServices(this IServiceCollection services)
        {
            services.AddScoped<ITorrentClientFactory, TorrentClientFactory>();
        }
    }
}
