﻿using DataAbstraction;

namespace Domain
{
    public class Credential : BaseIdEntity
    {
        public string Tracker { get; set; }
        public string Log { get; set; }
        public string Pass { get; set; }
        public string Cookie { get; set; }
        public string Passkey { get; set; }
    }
}
