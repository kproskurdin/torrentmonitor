﻿using DataAbstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain
{
    public class Warning : BaseIdEntity
    {
        public DateTime Time { get; set; }
        public string Where { get; set; }
        public string Reason { get; set; }
    }
}
