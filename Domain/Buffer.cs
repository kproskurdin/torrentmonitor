﻿using DataAbstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain
{
    public class Buffer : BaseIdEntity
    {
        public int UserId { get; set; }
        public string Section { get; set; }
        public int ThemeId { get; set; }
        public string Theme { get; set; }
        public bool Accept { get; set; }
        public bool Downloaded { get; set; }
        public bool @New { get; set; } = true;
        public string Tracker { get; set; }
    }
}
