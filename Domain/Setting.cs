﻿using DataAbstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain
{
    public class Setting : BaseIdEntity
    {
        public string Key { get; set; }
        public string Val { get; set; }
    }
}
