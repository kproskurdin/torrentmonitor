﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAbstraction;

namespace Domain
{
    public class StartData : BaseIdEntity
    {
        public DateTime StartDate { get; set; }
        public string ResultLog { get; set; }
    }
}
