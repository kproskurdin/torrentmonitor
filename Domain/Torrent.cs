﻿using DataAbstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain
{
    public class Torrent : BaseIdEntity
    {
        public string Tracker { get; set; }
        public string Name { get; set; }
        public int Hd { get; set; }
        public string Path { get; set; }
        public string Torrent_id { get; set; }
        public string Ep { get; set; }
        public DateTime? Timestamp { get; set; }
        public bool Auto_update { get; set; }
        public string Hash { get; set; }
        public string Script { get; set; }
    }
}
