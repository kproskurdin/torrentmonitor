﻿using DataAbstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain
{
    public class News : BaseIdEntity
    {
        public string Text { get; set; }
        public bool @New { get; set; } = true;
    }
}
