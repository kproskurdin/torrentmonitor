﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAbstraction;

namespace Domain
{
    public class SearchRequest : BaseIdEntity
    {
        public string Keyword { get; set; }

        public string Tracker { get; set; }

        public DateTime AddedDate { get; set; }
    }
}
