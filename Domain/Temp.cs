﻿using DataAbstraction;
using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Temp : BaseIdEntity
    {

        public string Name { get; set; }
        public string Path { get; set; }
        [MaxLength(50)]
        public string Hash { get; set; }
        public string Tracker { get; set; }
        public DateTime? Date { get; set; }
    }
}
