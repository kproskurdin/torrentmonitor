﻿using System;
using DataAbstraction;

namespace Domain
{
    public class SearchResult : BaseIdEntity
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public DateTime AddedDate { get; set; }

        public DateTime TrackerAddedDate { get; set; }
    }
}
