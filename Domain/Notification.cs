﻿using DataAbstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain
{
    public class Notification : BaseIdEntity
    {
        public string Service { get; set; }
        public string Address { get; set; }
        public string @Type { get; set; }
    }
}
