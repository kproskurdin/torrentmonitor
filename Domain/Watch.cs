﻿using DataAbstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain
{
    public class Watch : BaseIdEntity
    {
        public string Tracker { get; set; }
        public string Name { get; set; }
    }
}
