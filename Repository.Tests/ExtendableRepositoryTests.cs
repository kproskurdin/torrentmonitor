﻿using DataAbstraction;
using NSubstitute;
using Repository.Repositories;
using Xunit;

namespace Repository.Tests
{
    public class ExtendableRepositoryTests
    {
        [Fact]
        public void SaveChanges_call_data_context()
        {
            var ctx = Substitute.For<IDataContext>();
            var ctxFabric = Substitute.For<ITorrentMonitorDataContextFactory>();
            
            ctxFabric.Create().Returns(ctx);
            

            var t = new ExtendableRepository<TestEntity>(ctxFabric);
            t.SaveChanges();

            ctx.Received().SaveChanges();
        }
    }

    public class TestEntity : IEntity
    {
    }
}
