﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transmission.API.RPC;
using Transmission.API.RPC.Entity;
using Xunit;

namespace TranssmissionAPI.Tests
{
    public class ClientTests
    {
        const string FILE_PATH = "./Data/ubuntu-10.04.4-server-amd64.iso.torrent";
        const string HOST = "http://192.168.1.50:9091/transmission/rpc";
        const string SESSION_ID = "";

        Client client = new Client(HOST, SESSION_ID);

        [Fact]
        public void GetSessionInformation_throws_error_on_wrong_url()
        {
            client = new Client("fake_host_to_fail", SESSION_ID);

            Assert.Throws<AggregateException>(() => client.GetSessionInformation());
        }
    }
}
