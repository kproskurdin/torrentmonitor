﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Site.Services;
using Domain;
using Microsoft.AspNetCore.Mvc.Formatters;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Repository;
using Site.ViewModelFactories;
using Trackers;
using Scheduler;
using Updater;
using System.Linq;
using DataAbstraction;
using Microsoft.AspNetCore.Identity;
using TorrentClients;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Site
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            Configuration = CongigurationProvider.BuildConfiguration<Startup>(env);
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<TorrentMonitorDataContext>(options =>
    options.UseSqlite(Configuration.GetConnectionString("database")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<TorrentMonitorDataContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 8;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = false;
                options.Password.RequiredUniqueChars = 6;

                // Lockout settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 10;
                options.Lockout.AllowedForNewUsers = true;

                // User settings
                options.User.RequireUniqueEmail = true;
            });

            services.ConfigureApplicationCookie(options =>
            {
                // Cookie settings
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(30);
                // If the LoginPath isn't set, ASP.NET Core defaults 
                // the path to /Account/Login.
                options.LoginPath = "/Account/Login";
                // If the AccessDeniedPath isn't set, ASP.NET Core defaults 
                // the path to /Account/AccessDenied.
                options.AccessDeniedPath = "/Account/AccessDenied";
                options.SlidingExpiration = true;
            });



            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddDataProtection();

            // Add application services.
            services.AddTransient<IMapperService, MapperService>();

            //InRequestScope
            services.AddScoped<ITmMapper, TmMapper>();
            services.AddScoped<IEmailSender, AuthMessageSender>();
            services.AddScoped<ISmsSender, AuthMessageSender>();

            services.AddScoped<ICredentialsViewModelFactory, CredentialsViewModelFactory>();
            services.AddScoped<IAddViewModelFactory, AddViewModelFactory>();
            services.AddScoped<IShowTableVIewModelFactory, ShowTableVIewModelFactory>();
            services.AddScoped<IWatchViewModelFactory, WatchViewModelFactory>();
            services.AddScoped<ISettingsViewModelFactory, SettingsViewModelFactory>();
            services.AddScoped<IWarningsViewModelFactory, WarningsViewModelFactory>();
            services.AddScoped<ISearchViewModelFactory, SearchViewModelFactory>();

            services.AddRepositoryServices();
            services.AddTrackersServices();
            services.AddSchedulerServices();
            services.AddUpdaterServices();
            services.AddTorrentClientServices();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            // migrations
            /*using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>()
                       .CreateScope())
            {
                var ctx = serviceScope.ServiceProvider.GetService<TorrentMonitorDataContext>();
                ctx.Database.Migrate();
                ctx.EnsureSeedData(serviceScope.ServiceProvider.GetService<UserManager<ApplicationUser>>());
            }*/

        }
    }
}
