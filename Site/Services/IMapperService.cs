﻿using System;
using AutoMapper;
using Domain;
using Site.ViewModels.Action;
using Site.ViewModels.Include;

namespace Site.Services
{
    public interface IMapperService
    {
        IMapper Get();
    }

    public class MapperService : IMapperService
    {
        private static readonly MapperConfiguration Config;

        static MapperService()
        {
            Config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Credential, CredentialViewModel>();
                cfg.CreateMap<Credential, TrackerViewModel>();
                cfg.CreateMap<UpdateCredentialsViewModel, Credential>();
                cfg.CreateMap<Torrent, TorrentViewModel>()
                        .ForMember(dest => dest.IconName, 
                                    opt => opt.MapFrom(src => src.Tracker + ".ico"));
                cfg.CreateMap<Watch, WatchViewModel>();
                cfg.CreateMap<AddUserViewModel, Watch>();
                cfg.CreateMap<Setting, SettingViewModel>();
                cfg.CreateMap<Notification, NotificationViewModel>();
                cfg.CreateMap<Warning, WarningViewModel>();
                cfg.CreateMap<SearchRequestViewModel, SearchRequest>();
            });

        }

        public IMapper Get()
        {
            return Config.CreateMapper();
        }
    }

    public interface ITmMapper : IMapper
    {
        
    }

    public class TmMapper : ITmMapper
    {
        private readonly IMapper _mapper;

        public TmMapper(IMapperService mapperService)
        {
            this._mapper = mapperService.Get();
        }

        public TDestination Map<TDestination>(object source)
        {
            return _mapper.Map<TDestination>(source);
        }

        public TDestination Map<TDestination>(object source, Action<IMappingOperationOptions> opts)
        {
            return _mapper.Map<TDestination>(source, opts);
        }

        public TDestination Map<TSource, TDestination>(TSource source)
        {
            return _mapper.Map<TSource, TDestination>(source);
        }

        public TDestination Map<TSource, TDestination>(TSource source, Action opts)
        {
            throw new NotImplementedException();
        }

        public TDestination Map<TSource, TDestination>(TSource source, Action<IMappingOperationOptions<TSource, TDestination>> opts)
        {
            return _mapper.Map<TSource, TDestination>(source, opts);
        }

        public TDestination Map<TSource, TDestination>(TSource source, TDestination destination)
        {
            return _mapper.Map<TSource, TDestination>(source, destination);
        }

        public object Map(object source, Type sourceType, Type destinationType, Action opts)
        {
            throw new NotImplementedException();
        }

        public TDestination Map<TSource, TDestination>(TSource source, TDestination destination, Action opts)
        {
            throw new NotImplementedException();
        }

        public TDestination Map<TSource, TDestination>(TSource source, TDestination destination, Action<IMappingOperationOptions<TSource, TDestination>> opts)
        {
            return _mapper.Map<TSource, TDestination>(source, destination, opts);
        }

        public object Map(object source, Type sourceType, Type destinationType)
        {
            return _mapper.Map(source, sourceType, destinationType);
        }

        public object Map(object source, Type sourceType, Type destinationType, Action<IMappingOperationOptions> opts)
        {
            return _mapper.Map(source, sourceType, destinationType, opts);
        }

        public object Map(object source, object destination, Type sourceType, Type destinationType)
        {
            return _mapper.Map(source, destination, sourceType, destinationType);
        }

        public object Map(object source, object destination, Type sourceType, Type destinationType, Action<IMappingOperationOptions> opts)
        {
            return _mapper.Map(source, destination, sourceType, destinationType, opts);
        }

        public object Map(object source, object destination, Type sourceType, Type destinationType, Action opts)
        {
            throw new NotImplementedException();
        }

        public IConfigurationProvider ConfigurationProvider => _mapper.ConfigurationProvider;
        public Func<Type, object> ServiceCtor => _mapper.ServiceCtor;
    }
}