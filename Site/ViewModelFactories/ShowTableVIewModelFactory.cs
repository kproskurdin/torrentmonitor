﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Repository.Repositories;
using Site.Services;
using Site.ViewModels.Include;

namespace Site.ViewModelFactories
{
    public interface IShowTableVIewModelFactory
    {
        ShowTableVIewModel Get();
    }

    public class ShowTableVIewModelFactory : IShowTableVIewModelFactory
    {
        private readonly ITorrentRepository _torrentRepository;
        private readonly ITmMapper _mapper;
        private readonly IStartDataRepository _startDataRepository;

        public ShowTableVIewModelFactory(ITorrentRepository torrentRepository, ITmMapper mapper, IStartDataRepository startDataRepository)
        {
            _torrentRepository = torrentRepository;
            _mapper = mapper;
            _startDataRepository = startDataRepository;
        }

        public ShowTableVIewModel Get()
        {
            var lastRun = _startDataRepository.GetLast();

            return new ShowTableVIewModel
            {
                TorrentList = _torrentRepository.GetAll().Select(x => _mapper.Map<TorrentViewModel>(x)).ToList(),
                LastStart = lastRun?.StartDate
            };
        }
    }
}
