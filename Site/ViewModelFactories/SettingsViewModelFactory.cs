﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Repository.Repositories;
using Site.Services;
using Site.ViewModels.Action;
using Site.ViewModels.Include;

namespace Site.ViewModelFactories
{
    public interface ISettingsViewModelFactory
    {
        SettingsViewModel Get();

        MessageViewModel Save(UpdateSettingsViewModel model);
    }

    public class SettingsViewModelFactory : ISettingsViewModelFactory
    {
        private readonly ISettingsRepository _repository;
        private readonly INotificationRepository _notificationRepository;
        private readonly ITmMapper _mapper;

        public SettingsViewModelFactory(ISettingsRepository repository, ITmMapper mapper, INotificationRepository notificationRepository)
        {
            _repository = repository;
            _mapper = mapper;
            _notificationRepository = notificationRepository;
        }

        public SettingsViewModel Get()
        {
            return new SettingsViewModel
            {
                Settings = _repository.GetAll().Select(x => _mapper.Map<SettingViewModel>(x)).ToList(),
                Notifications = _notificationRepository.GetAll().Select(x => _mapper.Map<NotificationViewModel>(x)).ToList()
            };
        }

        public MessageViewModel Save(UpdateSettingsViewModel model)
        {
            _repository.GetByKey(nameof(model.ServerAddress)).Val = model.ServerAddress;
            _repository.GetByKey(nameof(model.Send)).Val = model.Send ? "1" : "0";
            _repository.GetByKey(nameof(model.SendUpdate)).Val = model.SendUpdate ? "1" : "0";
            _repository.GetByKey(nameof(model.SendWarning)).Val = model.SendWarning ? "1" : "0";
            _repository.GetByKey(nameof(model.SendUpdateService)).Val = model.SendUpdateService ? "1" : "0";
            _repository.GetByKey(nameof(model.SendWarningService)).Val = model.SendWarningService ? "1" : "0";
            _repository.GetByKey(nameof(model.Auth)).Val = model.Auth ? "1" : "0";
            _repository.GetByKey(nameof(model.Proxy)).Val = model.Proxy ? "1" : "0";
            _repository.GetByKey(nameof(model.ProxyType)).Val = model.ProxyType;
            _repository.GetByKey(nameof(model.ProxyAddress)).Val = model.ProxyAddress;
            _repository.GetByKey("useTorrent").Val = model.Torrent ? "1" : "0";
            _repository.GetByKey(nameof(model.TorrentClient)).Val = model.TorrentClient;
            _repository.GetByKey(nameof(model.TorrentAddress)).Val = model.TorrentAddress;
            _repository.GetByKey(nameof(model.TorrentLogin)).Val = model.TorrentLogin;
            _repository.GetByKey(nameof(model.TorrentPassword)).Val = model.TorrentPassword;
            _repository.GetByKey(nameof(model.PathToDownload)).Val = model.PathToDownload;
            _repository.GetByKey(nameof(model.DeleteDistribution)).Val = model.DeleteDistribution ? "1" : "0";
            _repository.GetByKey(nameof(model.DeleteOldFiles)).Val = model.DeleteOldFiles ? "1" : "0";
            _repository.GetByKey(nameof(model.Rss)).Val = model.Rss ? "1" : "0";
            _repository.GetByKey(nameof(model.Debug)).Val = model.Debug ? "1" : "0";

            _repository.SaveChanges();


            var sendUpdateServiceAddress = _notificationRepository.Get("notification", model.SendUpdateService ? "1" : "0");
            if (sendUpdateServiceAddress == null)
            {
                sendUpdateServiceAddress = new Notification {Service = "notification", Type = model.SendUpdateService ? "1" : "0" };
            }

            sendUpdateServiceAddress.Address = model.SendUpdateAddress;

            var sendWarningServiceAddress = _notificationRepository.Get("warning", model.SendWarningService ? "1" : "0");
            if (sendWarningServiceAddress == null)
            {
                sendWarningServiceAddress = new Notification { Service = "warning", Type = model.SendWarningService ? "1" : "0" };
            }

            sendWarningServiceAddress.Address = model.SendWarningAddress;

            return new MessageViewModel {Msg = "Настройки монитора обновлены."};
        }
    }
}
