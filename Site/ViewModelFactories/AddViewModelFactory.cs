﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using Domain;
using Repository.Repositories;
using Site.Services;
using Site.ViewModels.Action;
using Site.ViewModels.Include;
using Trackers;

namespace Site.ViewModelFactories
{
    public interface IAddViewModelFactory
    {
        AddViewModel Get();
        MessageViewModel AddTorent(TorrentAddModelViewModel model);
        MessageViewModel AddSerie(SerialAddViewModel model);
        TorrentViewModel GetEditModel(int id);
        MessageViewModel Update(UpdateViewModel model);
        MessageViewModel Delete(int id);
    }

    public class AddViewModelFactory : IAddViewModelFactory
    {
        private readonly ITorrentRepository _torrentRepository;
        private readonly ICredentialsRepository _credentialsRepository;
        private readonly ITorrentEngineActivator _torrentEngineActivator;
        private readonly ITmMapper _mapper;

        public AddViewModelFactory(ITorrentRepository torrentRepository, ICredentialsRepository credentialsRepository, ITorrentEngineActivator torrentEngineActivator, ITmMapper mapper)
        {
            _torrentRepository = torrentRepository;
            _credentialsRepository = credentialsRepository;
            _torrentEngineActivator = torrentEngineActivator;
            _mapper = mapper;
        }

        public AddViewModel Get()
        {
            return new AddViewModel
            {
                Paths = _torrentRepository.GetAllPaths().ToList()
            };
        }

        public MessageViewModel AddTorent(TorrentAddModelViewModel model)
        {
            MessageViewModel ret = new MessageViewModel();
            Uri url;

            try
            {
                url = new UriBuilder(model.Url).Uri;
            }
            catch (Exception)
            {
                ret.Error = true;
                ret.Msg = "Wrong Url";
                return ret;
            }
            

            var tracker = GetTrackerFromUrl(url);

            if (tracker == "lostfilm.tv" || tracker == "lostfilm-mirror" || tracker == "novafilm.tv" || tracker == "baibako.tv" || tracker == "newstudio.tv")
            {
                ret.Error = true;
                ret.Msg = "Это не форумный трекер. Добавьте как Сериал по его названию.";
            }
            else
            {
                if (tracker == "rutor.org")
                {
                    model.Url = "http://rutor.info" + url.PathAndQuery;
                }

                string threme = GetTherme(tracker, url);

                var cred = _credentialsRepository.GetByTracker(tracker);
                if (cred != null && cred.Log != null && cred.Pass != null)
                {
                    if (_torrentEngineActivator.Exist(tracker))
                    {
                        if (!string.IsNullOrEmpty(threme))
                        {
                            if (_torrentEngineActivator.GeTorrentEngine(tracker).CheckRule(threme))
                            {
                                if (!_torrentRepository.IfThereExist(tracker, threme))
                                {
                                    var name = model.Name;
                                    if (string.IsNullOrEmpty(name))
                                        name = model.Url;

                                    _torrentRepository.InsertAndSubmit(new Torrent
                                    {
                                       Tracker = tracker,
                                       Name = name,
                                       Path = model.Path,
                                       Torrent_id = threme,
                                       Auto_update = model.Update_header
                                    });

                                    ret.Error = false;
                                    ret.Msg = "Тема добавлена для мониторинга.";
                                }
                                else
                                {
                                    ret.Error = true;
                                    ret.Msg = $"Вы уже следите за данной темой на трекере <b>{tracker}</b>.";
                                }
                            }
                            else
                            {
                                ret.Error = true;
                                ret.Msg = "Неверная ссылка.";
                            }
                        }
                        else
                        {
                            ret.Error = true;
                            ret.Msg = "Неверная ссылка.";
                        }
                    }
                    else
                    {
                        ret.Error = true;
                        ret.Msg = $"Отсутствует модуль для трекера - <b>{tracker}</b>.";
                    }
                }
                else
                {
                    ret.Error = true;
                    ret.Msg = $"Вы не можете следить за этим сериалом на трекере - <b>{tracker}'</b>, пока не введёте свои учётные данные!";
                }

            }

            return ret;
        }

        public MessageViewModel AddSerie(SerialAddViewModel model)
        {
            MessageViewModel ret = new MessageViewModel();

            var tracker = model.Tracker;
            var cred = _credentialsRepository.GetByTracker(tracker);
            if (cred != null && cred.Log !=null && cred.Pass != null)
            {
                if (_torrentEngineActivator.Exist(tracker))
                {
                    if (!_torrentRepository.IfSerieExist(tracker, model.Name, model.Hd))
                    {
                        _torrentRepository.InsertAndSubmit(new Torrent
                        {
                            Tracker = tracker,
                            Name = model.Name,
                            Path = model.Path,
                            Hd = model.Hd
                        });

                        ret.Error = false;
                        ret.Msg = "Сериал добавлен для мониторинга.";
                    }
                    else
                    {
                        ret.Error = true;
                        ret.Msg = $"Вы уже следите за данным сериалом на этом трекере - <b>{tracker}</b>.";
                    }
                }
                else
                {
                    ret.Error = true;
                    ret.Msg = $"Отсутствует модуль для трекера - <b>{tracker}</b>.";
                }
            }
            else
            {
                ret.Error = true;
                ret.Msg =
                    $"Вы не можете следить за этим сериалом на трекере - <b>{tracker}</b>, пока не введёте свои учётные данные!";
            }

            return ret;
        }

        public TorrentViewModel GetEditModel(int id)
        {
            var torrent = _torrentRepository.GetById(id);
            var model = _mapper.Map<TorrentViewModel>(torrent);
            model.Paths = _torrentRepository.GetAllPaths().ToList();

            return model;
        }

        public MessageViewModel Update(UpdateViewModel model)
        {
            var ret = new MessageViewModel();
            var torrent = _torrentRepository.GetById(model.Id);

            if (model.Tracker == "lostfilm.tv" || model.Tracker == "lostfilm-mirror" || 
                model.Tracker == "novafilm.tv" || model.Tracker == "baibako.tv" || model.Tracker == "newstudio.tv")
            {
                torrent.Name = model.Name;
                torrent.Path = model.Path;
                torrent.Hd = model.Hd;
                torrent.Script = model.Script;
                if (model.Reset)
                {
                    torrent.Timestamp = null;
                    torrent.Ep = string.Empty;
                }

                _torrentRepository.SaveChanges();

                ret.Msg = "Сериал обновлён.";
                return ret;
            }

            Uri url;

            try
            {
                url = new UriBuilder(model.Url).Uri;
            }
            catch (Exception)
            {
                ret.Error = true;
                ret.Msg = "Wrong Url";
                return ret;
            }

            var tracker = GetTrackerFromUrl(url);

            var threme = GetTherme(tracker, url);

            var cred = _credentialsRepository.GetByTracker(tracker);

            if (cred != null && cred.Log != null && cred.Pass != null)
            {
                if (_torrentEngineActivator.Exist(tracker))
                {
                    if (!string.IsNullOrEmpty(threme))
                    {
                        if (_torrentEngineActivator.GeTorrentEngine(tracker).CheckRule(threme))
                        {
                            var name = model.Name;
                            if (string.IsNullOrEmpty(name))
                                name = model.Url;

                            torrent.Tracker = tracker;
                            torrent.Name = name;
                            torrent.Path = model.Path;
                            torrent.Torrent_id = threme;
                            torrent.Auto_update = model.Update;

                            _torrentRepository.SaveChanges();

                            ret.Error = false;
                            ret.Msg = "Тема обновлена.";
                        }
                        else
                        {
                            ret.Error = true;
                            ret.Msg = "Не верный ID темы.";
                        }
                    }
                    else
                    {
                        ret.Error = true;
                        ret.Msg = "Не верный ID темы.";
                    }
                }
                else
                {
                    ret.Error = true;
                    ret.Msg = $"Отсутствует модуль для трекера - <b>{tracker}</b>.";
                }
            }
            else
            {
                ret.Error = true;
                ret.Msg = $"Вы не можете следить за этим сериалом на трекере - <b>{tracker}'</b>, пока не введёте свои учётные данные!";
            }

            return ret;
        }

        public MessageViewModel Delete(int id)
        {
            var ret = new MessageViewModel();
            try
            {
                _torrentRepository.DeleteAndSubmit(_torrentRepository.GetById(id));
                ret.Msg = "Удалено.";
            }
            catch (Exception ex)
            {
                ret.Error = true;
                ret.Msg = ex.Message;
            }

            return ret;
        }

        private string GetTrackerFromUrl(Uri url)
        {
            var tracker = url.Host;
            tracker = Regex.Replace(tracker, @"/www\./", string.Empty);

            if (tracker == "tr.anidub.com")
                tracker = "anidub.com";
            else if (tracker == "nnm-club.me")
                tracker = "nnmclub.to";

            if (Regex.Match(tracker, @"/.*tor\.org|rutor\.info/").Success)
            {
                tracker = "rutor.org";
            }

            return tracker;
        }

        private string GetTherme(string tracker, Uri url)
        {
            string threme;
            if (tracker == "anidub.com")
                threme = url.AbsolutePath;
            else if (tracker == "animelayer.ru")
            {
                var path = url.AbsolutePath.Replace("/torrent", string.Empty);
                threme = Regex.Match(path, @"/\/(\w*)\/?/").Groups[1].Value;
            }
            else if (tracker == "casstudio.tv")
            {
                threme = url.Query.Split(new[] { "t=" }, StringSplitOptions.None)[1];
            }
            else if (tracker != "rutor.org")
            {
                threme = url.Query.Split('=')[1];
            }
            else
            {
                threme = Regex.Match(url.PathAndQuery, @"/\d{4,8}/").Groups[0].Value;
            }

            return threme;
        }
    }
}
