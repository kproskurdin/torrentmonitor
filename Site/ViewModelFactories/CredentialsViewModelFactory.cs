﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Repository.Repositories;
using Site.Controllers;
using Site.Services;
using Site.ViewModels.Action;
using Site.ViewModels.Include;

namespace Site.ViewModelFactories
{
    public interface ICredentialsViewModelFactory
    {
        CredentialsViewModel Get();
        void UpdateCredential(UpdateCredentialsViewModel model);
    }

    public class CredentialsViewModelFactory : ICredentialsViewModelFactory
    {
        private readonly ICredentialsRepository _repository;
        private readonly ITmMapper _mapper;

        public CredentialsViewModelFactory(ICredentialsRepository repository, ITmMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public CredentialsViewModel Get()
        {
            var trackers = _repository.GetAll().ToList();

            return new CredentialsViewModel
            {
                Trackers = trackers.Select(x => _mapper.Map<TrackerViewModel>(x)).ToList(),
                Credentials = trackers.Select(x => _mapper.Map<CredentialViewModel>(x)).ToList()
            };
        }

        public void UpdateCredential(UpdateCredentialsViewModel model)
        {
            var cred = _repository.GetById(model.Id);
            if(cred == null) throw new Exception($"Credential with id {model.Id} not found");

            cred = _mapper.Map(model, cred);

            _repository.SaveChanges();
        }
    }
}
