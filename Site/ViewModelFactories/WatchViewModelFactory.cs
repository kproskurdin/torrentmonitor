﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Repository.Repositories;
using Site.Services;
using Site.ViewModels.Action;
using Site.ViewModels.Include;
using Trackers;

namespace Site.ViewModelFactories
{
    public interface IWatchViewModelFactory
    {
        ShowWatchingViewModel Get();

        MessageViewModel Add(AddUserViewModel model);
    }

    public class WatchViewModelFactory : IWatchViewModelFactory
    {
        private readonly IWatchRepository _repository;
        private readonly ICredentialsRepository _credentialsRepository;
        private readonly ITmMapper _mapper;
        private readonly ITorrentEngineActivator _torrentEngineActivator;

        public WatchViewModelFactory(IWatchRepository repository, ITmMapper mapper, ICredentialsRepository credentialsRepository, ITorrentEngineActivator torrentEngineActivator)
        {
            _repository = repository;
            _mapper = mapper;
            _credentialsRepository = credentialsRepository;
            _torrentEngineActivator = torrentEngineActivator;
        }

        public ShowWatchingViewModel Get()
        {
            return new ShowWatchingViewModel
            {
                Watchs = _repository.GetAll().Select(x => _mapper.Map<WatchViewModel>(x)).ToList()
            };
        }

        public MessageViewModel Add(AddUserViewModel model)
        {
            MessageViewModel ret = new MessageViewModel();

            var tracker = model.Tracker;
            var cred = _credentialsRepository.GetByTracker(tracker);
            if (cred != null && cred.Log != null && cred.Pass != null)
            {
                if (_torrentEngineActivator.Exist(tracker))
                {
                    if (!_repository.IfExist(tracker, model.Name))
                    {
                        _repository.InsertAndSubmit(new Watch
                        {
                            Tracker = tracker,
                            Name = model.Name
                        });

                        ret.Error = false;
                        ret.Msg = "Пользователь добавлен для мониторинга.";
                    }
                    else
                    {
                        ret.Error = true;
                        ret.Msg = $"Вы уже следите за данным пользователем на этом трекере - <b>{tracker}</b>.";
                    }
                }
                else
                {
                    ret.Error = true;
                    ret.Msg = $"Отсутствует модуль для трекера - <b>{tracker}</b>.";
                }
            }
            else
            {
                ret.Error = true;
                ret.Msg =
                    $"Вы не можете следить за этим сериалом на трекере - <b>{tracker}</b>, пока не введёте свои учётные данные!";
            }

            return ret;
        }
    }
}
