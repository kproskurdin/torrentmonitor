﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Repository.Repositories;
using Site.Services;
using Site.ViewModels.Action;

namespace Site.ViewModelFactories
{
    public interface ISearchViewModelFactory
    {
        MessageViewModel Add(SearchRequestViewModel model);
    }

    public class SearchViewModelFactory : ISearchViewModelFactory
    {
        private readonly ITmMapper _mapper;
        private readonly ISearchRequestRepository _repo;

        public SearchViewModelFactory(ITmMapper mapper, ISearchRequestRepository repo)
        {
            _mapper = mapper;
            _repo = repo;
        }

        public MessageViewModel Add(SearchRequestViewModel model)
        {
            var sr = _mapper.Map<SearchRequest>(model);
            sr.AddedDate = DateTime.Now;

            _repo.InsertAndSubmit(sr);

            return new MessageViewModel
            {
                Msg = "saved"
            };
        }
    }
}
