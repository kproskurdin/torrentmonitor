﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Repository.Repositories;
using Site.Services;
using Site.ViewModels.Include;

namespace Site.ViewModelFactories
{
    public interface IWarningsViewModelFactory
    {
        WarningsViewModel Get();
    }

    public class WarningsViewModelFactory : IWarningsViewModelFactory
    {
        private readonly ITmMapper _mapper;
        private readonly IWarningRepository _repository;


        public WarningsViewModelFactory(ITmMapper mapper, IWarningRepository repository)
        {
            _mapper = mapper;
            _repository = repository;
        }

        public WarningsViewModel Get()
        {
            return new WarningsViewModel
            {
                Warnings = _repository.GetTop(100).Select(x => _mapper.Map<WarningViewModel>(x)).ToList()
            };
        }
    }
}
