﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Site.ViewModels.Include
{
    public class CredentialsViewModel
    {
        public List<CredentialViewModel> Credentials { get; set; } = new List<CredentialViewModel>();

        public List<TrackerViewModel> Trackers { get; set; } = new List<TrackerViewModel>();
    }
}
