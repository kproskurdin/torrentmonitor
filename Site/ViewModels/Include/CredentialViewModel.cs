﻿namespace Site.ViewModels.Include
{
    public class CredentialViewModel
    {
        public int Id { get; set; }
        public string Tracker { get; set; }
        public string Log { get; set; }
        public string Pass { get; set; }
        public string Cookie { get; set; }
        public string Passkey { get; set; }
    }
}