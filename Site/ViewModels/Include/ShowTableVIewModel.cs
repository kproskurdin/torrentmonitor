﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Site.ViewModels.Include
{
    public class ShowTableVIewModel
    {
        public List<TorrentViewModel> TorrentList { get; set; } = new List<TorrentViewModel>();
        public DateTime? LastStart { get; set; }
    }
}
