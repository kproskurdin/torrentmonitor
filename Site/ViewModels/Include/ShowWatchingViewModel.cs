﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Site.ViewModels.Include
{
    public class ShowWatchingViewModel
    {
        public List<WatchViewModel> Watchs { get; set; } = new List<WatchViewModel>();
    }
}
