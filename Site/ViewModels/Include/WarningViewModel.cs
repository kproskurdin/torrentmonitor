﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Site.ViewModels.Include
{
    public class WarningsViewModel
    {
        public List<WarningViewModel> Warnings { get; set; } = new List<WarningViewModel>();
    }

    public class WarningViewModel
    {
        public int Id { get; set; }
        public DateTime Time { get; set; }
        public string Where { get; set; }
        public string Reason { get; set; }
    }
}
