﻿using System.Collections.Generic;

namespace Site.ViewModels.Include
{
    public class WatchViewModel
    {
        public int Id { get; set; }
        public string Tracker { get; set; }
        public string Name { get; set; }

        public List<BufferViewModel> Themes { get; set; } = new List<BufferViewModel>();
    }

    public class BufferViewModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Section { get; set; }
        public int ThemeId { get; set; }
        public string Theme { get; set; }
        public bool Accept { get; set; }
        public bool Downloaded { get; set; }
        public bool @New { get; set; } = true;
        public string Tracker { get; set; }
    }
}