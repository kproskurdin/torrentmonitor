﻿namespace Site.ViewModels.Include
{
    public class TrackerViewModel
    {
        public int Id { get; set; }
        public string Tracker { get; set; }
    }
}