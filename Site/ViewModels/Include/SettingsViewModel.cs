﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;

namespace Site.ViewModels.Include
{
    public class SettingsViewModel
    {
        public List<SettingViewModel> Settings { get; set; }
        public List<NotificationViewModel> Notifications { get; set; }

        public string GetSettingStringValue(string name)
        {
            return Settings.First(x => x.Key == name).Val;
        }

        public bool GetSettingBoolValue(string name)
        {
            var strtVal = Settings.First(x => x.Key == name).Val;
            return strtVal == "1";
        }

        public IEnumerable<NotificationViewModel> GetNotificationList(string type)
        {
            return Notifications.Where(x => x.Type == type);
        }

        public IEnumerable<SelectListItem> GetNotificationListOptions(string type, string val)
        {
            return GetNotificationList(type).Select(x => new SelectListItem() { Value = x.Id.ToString(), Text = x.Service, Selected =  val == x.Id.ToString()});
        }
    }


    public class SettingViewModel
    {
        public string Key { get; set; }
        public string Val { get; set; }
    }

    public class NotificationViewModel
    {
        public int Id { get; set; }
        public string Service { get; set; }
        public string Address { get; set; }
        public string @Type { get; set; }
    }
}
