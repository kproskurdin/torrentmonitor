﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Site.ViewModels.Include
{
    public class AddViewModel
    {
        public List<string> Paths { get; set; } = new List<string>();
    }
}
