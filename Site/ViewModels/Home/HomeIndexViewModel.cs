﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TorrentMonitor.ViewModels.Home
{
    public class HomeIndexViewModel
    {
        public bool Update { get; set; }
        public string Version { get; set; }

        public int WarningsCount { get; set; }
        public int NewsCount { get; set; }
    }

    public class WarningViewModel
    {

    }

    public class NewsViewModel
    {

    }
}
