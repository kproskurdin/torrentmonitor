namespace Site.ViewModels.Action
{
    public class TorrentAddModelViewModel
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string Path { get; set; }
        public bool Update_header { get; set; }
    }
}