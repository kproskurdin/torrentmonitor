namespace Site.ViewModels.Action
{
    public class AddUserViewModel
    {
        public string Tracker { get; set; }
        public string Name { get; set; }
    }
}