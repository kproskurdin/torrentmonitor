namespace Site.ViewModels.Action
{
    public class UpdateViewModel
    {
        public int Id { get; set; }
        public string Tracker { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public bool Update { get; set; }
        public string Path { get; set; }
        public string Script { get; set; }
        public int Hd { get; set; }
        public bool Reset { get; set; }
    }
}