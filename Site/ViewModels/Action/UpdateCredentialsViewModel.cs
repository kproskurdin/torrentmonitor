namespace Site.ViewModels.Action
{
    public class UpdateCredentialsViewModel
    {
        public int Id { get; set; }
        public string Log { get; set; }
        public string Pass { get; set; }
        public string PassKey { get; set; }
    }
}