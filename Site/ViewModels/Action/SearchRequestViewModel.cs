﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Site.ViewModels.Action
{
    public class SearchRequestViewModel
    {
        public string Keyword { get; set; }

        public string Tracker { get; set; }
    }
}
