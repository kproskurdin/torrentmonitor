namespace Site.ViewModels.Action
{
    public class SerialAddViewModel
    {
        public string Tracker { get; set; }
        public string Name { get; set; }
        public int Hd { get; set; }
        public string Path { get; set; }
    }
}