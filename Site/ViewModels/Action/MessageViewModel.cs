namespace Site.ViewModels.Action
{
    public class MessageViewModel
    {
        public string Msg { get; set; }
        public bool Error { get; set; }
    }
}