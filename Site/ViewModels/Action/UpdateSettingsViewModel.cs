namespace Site.ViewModels.Action
{
    public class UpdateSettingsViewModel
    {
        public string ServerAddress { get; set; }
        public bool Send { get; set; }
        public bool SendUpdate { get; set; }
        public bool SendWarning { get; set; }
        public bool SendUpdateService { get; set; }
        public string SendUpdateAddress { get; set; }
        public bool SendWarningService { get; set; }
        public string SendWarningAddress { get; set; }
        public bool Auth { get; set; }
        public bool Proxy { get; set; }
        public string ProxyType { get; set; }
        public string ProxyAddress { get; set; }
        public bool Torrent { get; set; }
        public string TorrentClient { get; set; }
        public string TorrentAddress { get; set; }
        public string TorrentLogin { get; set; }
        public string TorrentPassword { get; set; }
        public string PathToDownload { get; set; }
        public bool DeleteDistribution { get; set; }
        public bool DeleteOldFiles { get; set; }
        public bool Rss { get; set; }
        public bool Debug { get; set; }
    }
}