﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TorrentMonitor.ViewModels.Home;
using Updater;

namespace Site.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly IUpdater _updater;

        public HomeController(IUpdater updater)
        {
            _updater = updater;
        }

        public IActionResult Index()
        {
            var model = new HomeIndexViewModel {Version = _updater.GetCurrentVersion().ToString()};

            return View(model);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
