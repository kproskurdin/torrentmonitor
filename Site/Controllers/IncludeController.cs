using Scheduler;
using Site.ViewModelFactories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;

namespace Site.Controllers
{
    [Route("include")]
    [Authorize]
    public class IncludeController : Controller
    {
        private readonly ICredentialsViewModelFactory _credentialsViewModelFactory;
        private readonly IAddViewModelFactory _addViewModelFactory;
        private readonly IShowTableVIewModelFactory _showTableVIewModelFactory;
        private readonly IWatchViewModelFactory _watchViewModelFactory;
        private readonly ISettingsViewModelFactory _settingsViewModelFactory;
        private readonly IWarningsViewModelFactory _warningsViewModelFactory;
        private readonly ISchedulerEngine _schedulerEngine;

        public IncludeController(ICredentialsViewModelFactory credentialsViewModelFactory, IAddViewModelFactory addViewModelFactory, IShowTableVIewModelFactory showTableVIewModelFactory, IWatchViewModelFactory watchViewModelFactory, ISettingsViewModelFactory settingsViewModelFactory, IWarningsViewModelFactory warningsViewModelFactory, ISchedulerEngine schedulerEngine)
        {
            _credentialsViewModelFactory = credentialsViewModelFactory;
            _addViewModelFactory = addViewModelFactory;
            _showTableVIewModelFactory = showTableVIewModelFactory;
            _watchViewModelFactory = watchViewModelFactory;
            _settingsViewModelFactory = settingsViewModelFactory;
            _warningsViewModelFactory = warningsViewModelFactory;
            _schedulerEngine = schedulerEngine;
        }

        [HttpGet("show-table")]
        public IActionResult ShowTable()
        {
            return View(_showTableVIewModelFactory.Get());
        }

        [HttpGet("show-watching")]
        public IActionResult ShowWatching()
        {
            return View(_watchViewModelFactory.Get());
        }

        [HttpGet("form")]
        public IActionResult Form(int id)
        {
            return View(_addViewModelFactory.GetEditModel(id));
        }

        [HttpGet("add")]
        public IActionResult Add()
        {
            return View(_addViewModelFactory.Get());
        }

        [HttpGet("credentials")]
        public IActionResult Credentials()
        {
            return View(_credentialsViewModelFactory.Get());
        }

        [HttpGet("settings")]
        public IActionResult Settings()
        {
            return View(_settingsViewModelFactory.Get());
        }

        [HttpGet("show-warnings")]
        public IActionResult ShowWarnings()
        {
            return View(_warningsViewModelFactory.Get());
        }

        [HttpGet("check")]
        public IActionResult Check()
        {
            return View();
        }

        [HttpGet("execution")]
        public async Task<IActionResult> Execution()
        {
            var model = await _schedulerEngine.Run();
            return View(model);
        }

        [HttpGet("news")]
        public IActionResult News()
        {
            return View();
        }

        [HttpGet("help")]
        public IActionResult Help()
        {
            return View();
        }
    }
}
