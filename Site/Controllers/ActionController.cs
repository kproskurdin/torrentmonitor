using System;
using Site.ViewModelFactories;
using Site.ViewModels.Action;
using Microsoft.AspNetCore.Mvc;

namespace Site.Controllers
{
    [Produces("application/json")]
    [Route("api/Action")]
    public class ActionController : Controller
    {
        private readonly ICredentialsViewModelFactory _credentialsViewModelFactory;
        private readonly IAddViewModelFactory _addViewModelFactory;
        private readonly IWatchViewModelFactory _watchViewModelFactory;
        private readonly ISettingsViewModelFactory _settingsViewModelFactory;
        private readonly ISearchViewModelFactory _searchViewModelFactory;

        public ActionController(ICredentialsViewModelFactory credentialsViewModelFactory, IAddViewModelFactory addViewModelFactory, IWatchViewModelFactory watchViewModelFactory, ISettingsViewModelFactory settingsViewModelFactory, ISearchViewModelFactory searchViewModelFactory)
        {
            _credentialsViewModelFactory = credentialsViewModelFactory;
            _addViewModelFactory = addViewModelFactory;
            _watchViewModelFactory = watchViewModelFactory;
            _settingsViewModelFactory = settingsViewModelFactory;
            _searchViewModelFactory = searchViewModelFactory;
        }

        [HttpPost("torrent-add")]
        public MessageViewModel TorrentAdd(TorrentAddModelViewModel model)
        {
            return _addViewModelFactory.AddTorent(model);
        }

        [HttpPost("serial-add")]
        public MessageViewModel SerialAdd(SerialAddViewModel model)
        {
            return _addViewModelFactory.AddSerie(model);
        }

        [HttpPost("update")]
        public MessageViewModel Update(UpdateViewModel model)
        {
            return _addViewModelFactory.Update(model);
        }

        [HttpPost("search-add")]
        public MessageViewModel AddSearch(SearchRequestViewModel model)
        {
            return _searchViewModelFactory.Add(model);
        }

        [HttpPost("user-add")]
        public MessageViewModel AddUser(AddUserViewModel model)
        {
            return _watchViewModelFactory.Add(model);
        }

        [HttpPost("threme-clear")]
        public MessageViewModel ThremeClear()
        {
            throw new NotImplementedException();
        }

        [HttpPost("update-credentials")]
        public MessageViewModel UpdateCredentials(UpdateCredentialsViewModel model)
        {
            try
            {
                _credentialsViewModelFactory.UpdateCredential(model);

                return new MessageViewModel {Msg = "Updated"};
            }
            catch (Exception ex)
            {
                return new MessageViewModel { Msg = ex.Message, Error = true };
            }
        }

        [HttpPost("update-settings")]
        public MessageViewModel UpdateSettings(UpdateSettingsViewModel model)
        {
            return _settingsViewModelFactory.Save(model);
        }

        [HttpPost("system-update")]
        public MessageViewModel SystemUpdate()
        {
            throw new NotImplementedException();
        }

        [HttpPost("delete-user")]
        public MessageViewModel DeleteUser([FromBody]int user_id)
        {
            throw new NotImplementedException();
        }

        [HttpPost("delete-from-buffer")]
        public MessageViewModel DeleteFromBuffer([FromBody]int id)
        {
            throw new NotImplementedException();
        }

        [HttpPost("transfer-from-buffer")]
        public MessageViewModel TransferFromBuffer([FromBody]int id)
        {
            throw new NotImplementedException();
        }

        [HttpPost("threme-add")]
        public MessageViewModel ThremeAdd(ThremeAddViewModel model)
        {
            throw new NotImplementedException();
        }

        [HttpPost("del")]
        public MessageViewModel Delete(IdModel model)
        {
            return _addViewModelFactory.Delete(model.Id);
        }

        [HttpPost("mark-news")]
        public MessageViewModel MarkNews([FromBody]int id)
        {
            throw new NotImplementedException();
        }
    }

    public class IdModel
    {
        public int Id { get; set; }
    }
}