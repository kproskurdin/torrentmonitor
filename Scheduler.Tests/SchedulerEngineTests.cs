﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using NSubstitute;
using Repository;
using Repository.Repositories;
using Trackers;
using Xunit;

namespace Scheduler.Tests
{
    public class SchedulerEngineTests
    {
        [Fact]
        public void Run_returns()
        {
            var torrentRepository = Substitute.For<ITorrentRepository>();
            var engineActivator = Substitute.For<ITorrentEngineActivator>();
            var warningRepository = Substitute.For<IWarningRepository>();
            var startDataRepository = Substitute.For<IStartDataRepository>();
            var repFactory = Substitute.For<IRepositoryFactory>();

            torrentRepository.GetAll().Returns(new List<Torrent>());

            var se = new SchedulerEngine(torrentRepository, engineActivator, warningRepository, startDataRepository, repFactory);

            var res = se.Run().Result;

            Assert.Equal(2, res.Count);
        }
    }
}
