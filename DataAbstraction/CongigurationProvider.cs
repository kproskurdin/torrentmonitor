﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DataAbstraction
{
    public static class CongigurationProvider
    {
        private static IConfigurationRoot Configuration { get; set; } = null;

        public static IConfigurationRoot BuildConfiguration<T>(IHostingEnvironment env) where T: class
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see http://go.microsoft.com/fwlink/?LinkID=532709
                builder.AddUserSecrets<T>();

                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                //builder.AddApplicationInsightsSettings(developerMode: true);
            }

            //builder.AddEnvironmentVariables();

            Configuration = builder.Build();

            return GetConfiguration();
        }

        public static void SetConfiguration(IConfigurationRoot configuration)
        {
            Configuration = configuration;
        }

        public static IConfigurationRoot GetConfiguration()
        {
            if(Configuration == null) throw new NullReferenceException("Configuration is null");

            return Configuration;
        }
    }
}
