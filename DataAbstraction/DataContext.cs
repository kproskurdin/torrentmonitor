﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace DataAbstraction
{
    public interface IEntity
    {

    }

    public interface IDataContext
    {
        DbSet<T> Set<T>() where T : class;

        int SaveChanges();
        //void DiscardChanges(object entity);

        List<T> GetChangedEntities<T>() where T : class;
    }
}