﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NSubstitute;
using Repository.Repositories;
using Site.Services;
using Site.ViewModelFactories;
using Trackers;
using Xunit;

namespace Site.Tests
{
    public class AddViewModelFactoryTests
    {
        [Fact]
        public void Get_returns_model()
        {
            var torrentRepository = Substitute.For<ITorrentRepository>();
            var credentialsRepository = Substitute.For<ICredentialsRepository>();
            var torrentEngineActivator = Substitute.For<ITorrentEngineActivator>();
            var mapper = Substitute.For<ITmMapper>();

            var modelFactory = new AddViewModelFactory(torrentRepository, credentialsRepository, 
                torrentEngineActivator, mapper);

            var model = modelFactory.Get();

            Assert.NotNull(model);
        }
    }
}
