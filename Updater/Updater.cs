﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Updater
{
    public interface IUpdater
    {
        void Run();
        SemanticVersionInfo GetCurrentVersion();
        SemanticVersionInfo GetLastVersion();
    }

    public class Updater : IUpdater
    {
        private const string Url = "https://bitbucket.org/kproskurdin/torrentmonitor/downloads";

        public void Run()
        {
            var lastVersion = GetLastVersion(Url);

            if (lastVersion == SemanticVersionInfo.MinValue) return;

            var currentVersion = GetCurrentVersion();

            if (lastVersion > currentVersion)
            {
                Update(Url, lastVersion);
            }
        }

        private void Update(string url, SemanticVersionInfo version)
        {
            var content = Get(url);
            var m = Regex.Match(content, $@"/kproskurdin/torrentmonitor/downloads/(.*?){version.ToString()}.zip");
            if(!m.Success) throw new Exception("Wrong version");

            Console.WriteLine("Update is in progress, please wait...");

            var u = new Uri(url);

            Console.Write("Downloading update... ");

            var zip = GetBinary($"{u.Scheme}://{u.Host}{m.Groups[0]}");

            Console.WriteLine("Done");

            var tempDir = "update_temp";
            var tempDirName = $@".\{tempDir}";
            var archiveFielName = $@"{tempDirName}\update.{version.ToString()}.zip";

            if (!Directory.Exists(tempDirName)) Directory.CreateDirectory(tempDirName);

            File.WriteAllBytes(archiveFielName, zip);

            Console.Write("Extract archive... ");

            ZipFile.ExtractToDirectory(archiveFielName, tempDirName);

            File.Delete(archiveFielName);

            Console.WriteLine("Done");

            Console.Write("Cleanup directory... ");

            foreach (var directory in Directory.GetDirectories("."))
            {
                if (directory.Contains(tempDir)) continue;

                Directory.Delete(directory, true);
            }

            foreach (var file in Directory.GetFiles("."))
            {
                try
                {
                    if (file.EndsWith(".sqllite")) continue;

                    File.Delete(file);
                }
                catch (Exception)
                {
                    
                }
            }

            Console.WriteLine("Done");

            Console.Write("Copy new files... ");

            foreach (string dirPath in Directory.GetDirectories(tempDirName, "*", SearchOption.AllDirectories))
            {
                Directory.CreateDirectory(dirPath.Replace(tempDirName, "."));
            }

            foreach (string newPath in Directory.GetFiles(tempDirName, "*.*", SearchOption.AllDirectories))
            {
                try
                { 
                    File.Copy(newPath, newPath.Replace($"{tempDir}\\", String.Empty), true);
                }
                catch (Exception)
                {
                    
                }
            }

            Console.WriteLine("Done");

            Console.WriteLine("Update is done");
        }

        public SemanticVersionInfo GetCurrentVersion()
        {
            try
            {
                var version = File.ReadAllText(".\\version.txt");
                if (String.IsNullOrEmpty(version)) return SemanticVersionInfo.MinValue;

                SemanticVersionInfo ver;

                return SemanticVersionInfo.TryParse(version, out ver) ? ver : SemanticVersionInfo.MinValue;
            }
            catch (Exception)
            {
                return SemanticVersionInfo.MinValue;
            }
        }

        public SemanticVersionInfo GetLastVersion()
        {
            return GetLastVersion(Url);
        }

        private SemanticVersionInfo GetLastVersion(string url)
        {
            var content = Get(url);

            var matches = Regex.Matches(content, @"\d{1,2}\.\d{1,2}\.\d{1,2}(-([A-Z,a-z,0-9]*))");
            if (matches == null || matches.Count == 0) return SemanticVersionInfo.MinValue;

            var versions = new List<SemanticVersionInfo>();
            foreach (Match m in matches)
            {
                versions.Add(SemanticVersionInfo.Parse(m.Groups[0].Value));
            }

            return versions.Max();
        }

        private static string Get(string url)
        {
            string content;

            using (var client = new HttpClient())
            {
                HttpResponseMessage result = client.GetAsync(url).Result;

                result.EnsureSuccessStatusCode();

                var encoding = Encoding.UTF8;

                using (var responseStream = result.Content.ReadAsStreamAsync().Result)
                {
                    if (result.Content.Headers.ContentEncoding.FirstOrDefault() == "gzip")
                    {
                        using (var decompressedStream = new GZipStream(responseStream, CompressionMode.Decompress))
                        {
                            using (var streamReader = new StreamReader(decompressedStream, encoding))
                            {
                                content = streamReader.ReadToEnd();
                            }
                        }
                    }
                    else
                    {
                        using (var streamReader = new StreamReader(responseStream, encoding))
                        {
                            content = streamReader.ReadToEnd();
                        }
                    }
                }
            }

            return content;
        }

        private static byte[] GetBinary(string url)
        {
            var ms = new MemoryStream();

            using (var client = new HttpClient())
            {
                HttpResponseMessage result = client.GetAsync(url).Result;

                result.EnsureSuccessStatusCode();

                var encoding = Encoding.UTF8;

                using (var responseStream = result.Content.ReadAsStreamAsync().Result)
                {
                    if (result.Content.Headers.ContentEncoding.FirstOrDefault() == "gzip")
                    {
                        using (var decompressedStream = new GZipStream(responseStream, CompressionMode.Decompress))
                        {
                            decompressedStream.CopyTo(ms);
                        }
                    }
                    else
                    {
                        responseStream.CopyTo(ms);
                    }
                }
            }

            return ms.ToArray();
        }
    }
}
