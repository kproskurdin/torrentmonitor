using System;
using System.Text.RegularExpressions;

namespace Updater
{
    public struct SemanticVersionInfo : IComparable
    {
        public int Major { get; set; }
        public int Minor { get; set; }
        public int Patch { get; set; }
        public string Label { get; set; }

        public static bool TryParse(string input, out SemanticVersionInfo version)
        {
            try
            {
                version = Parse(input);
                return true;
            }
            catch (Exception)
            {
                version = MinValue;
                return false;
            }
        }

        private const string Pattern = @"(\d{1,2})\.(\d{1,2})\.(\d{1,2})(-([A-Z,a-z,0-9]*))?";

        public static SemanticVersionInfo MinValue = new SemanticVersionInfo
        {
            Major = 0,
            Minor = 0,
            Patch = 0,
            Label = null
        };

        public static SemanticVersionInfo Parse(string input)
        {
            var match = Regex.Match(input, Pattern);

            if (!match.Success) throw new FormatException("Wrong Format");

            return new SemanticVersionInfo
            {
                Major = int.Parse(match.Groups[1].Value),
                Minor = int.Parse(match.Groups[2].Value),
                Patch = int.Parse(match.Groups[3].Value),
                Label = match.Groups[5].Value
            };
        }

        public int CompareTo(object obj)
        {
            var o2 = (SemanticVersionInfo) obj;

            if (Major == o2.Major && Minor == o2.Minor && string.Equals(Label, o2.Label)) return 0;

            if (Major > o2.Major) return 1;
            if (Major < o2.Major) return -1;

            if (Minor > o2.Minor) return 1;
            if (Minor < o2.Minor) return -1;
            
            
            return Label.ComparerNatural(o2.Label);
        }

        public override bool Equals(object obj)
        {
            return CompareTo(obj) == 0;
        }

        public bool Equals(SemanticVersionInfo other)
        {
            return CompareTo(other) == 0;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Major;
                hashCode = (hashCode*397) ^ Minor;
                hashCode = (hashCode*397) ^ Patch;
                hashCode = (hashCode*397) ^ (Label?.GetHashCode() ?? 0);
                return hashCode;
            }
        }

        public static bool operator ==(SemanticVersionInfo a, SemanticVersionInfo b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(SemanticVersionInfo a, SemanticVersionInfo b)
        {
            return !a.Equals(b);
        }

        public static bool operator >(SemanticVersionInfo a, SemanticVersionInfo b)
        {
            return a.CompareTo(b) > 0;
        }

        public static bool operator <(SemanticVersionInfo a, SemanticVersionInfo b)
        {
            return a.CompareTo(b) < 0;
        }

        public static bool operator >=(SemanticVersionInfo a, SemanticVersionInfo b)
        {
            return a.CompareTo(b) >= 0;
        }

        public static bool operator <=(SemanticVersionInfo a, SemanticVersionInfo b)
        {
            return a.CompareTo(b) <= 0;
        }

        public override string ToString()
        {
            return $"{Major}.{Minor}.{Patch}{(!string.IsNullOrWhiteSpace(Label) ? $"-{Label}" : string.Empty)}";
        }
    }
}