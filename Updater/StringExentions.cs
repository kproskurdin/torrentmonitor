using System.Linq;
using System.Text.RegularExpressions;

namespace Updater
{
    public static class StringExentions
    {
        private static object Convert(string str)
        {
            int t;
            return int.TryParse(str, out t) ? (object)t : (object)str;
        }

        public static int ComparerNatural(this string a, string b)
        {
            if (a == null && b == null) return 0;
            if (a == null) return -1;
            if (b == null) return 1;

            return new EnumerableComparer<object>().Compare(
                Regex.Split(a.Replace(" ", ""), "([0-9]+)").Select(Convert),
                Regex.Split(b.Replace(" ", ""), "([0-9]+)").Select(Convert));
        }
    }
}