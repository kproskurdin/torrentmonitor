﻿using System;

namespace Updater
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                (new Updater()).Run();
            }
            catch(Exception ex)
            {
                PrintError(ex);
            }
        }

        private static void PrintError(Exception ex)
        {
            if (ex == null) return;

            Console.WriteLine(ex.Message);
            Console.WriteLine(ex.StackTrace);

            PrintError(ex.InnerException);
        }
    }
}