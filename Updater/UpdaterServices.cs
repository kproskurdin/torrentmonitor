﻿using Microsoft.Extensions.DependencyInjection;

namespace Updater
{
    public static class UpdaterServices
    {
        public static void AddUpdaterServices(this IServiceCollection services)
        {
            services.AddScoped<IUpdater, Updater>();
        }
    }
}
