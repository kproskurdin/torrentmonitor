﻿using System;
using System.Net.Http;
using Trackers.Helpers;
using Xunit;

namespace Trackers.Tests
{
    public class UrlHelperTests
    {
        [Fact]
        public void GetContent_throws_error_on_null_url()
        {
            var h = new UrlHelper();
            Assert.Throws<AggregateException>(() => h.GetContent(null, HttpMethod.Get, null, null, cookies: null).Result);
        }

        [Fact]
        public void GetContent_throws_error_on_empty_url()
        {
            var h = new UrlHelper();
            Assert.Throws<AggregateException>(() => h.GetContent(string.Empty, HttpMethod.Get, null, null, cookies: null).Result);
        }
    }
}
